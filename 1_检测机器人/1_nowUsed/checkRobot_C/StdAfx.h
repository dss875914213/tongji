// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__1139F678_9740_4111_9E6F_15BCE64CD0ED__INCLUDED_)
#define AFX_STDAFX_H__1139F678_9740_4111_9E6F_15BCE64CD0ED__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

//#define DLL_EXPORT

#ifdef	DLL_EXPORT
#define CCARDLIB_CLASS	__declspec(dllexport)
#define CCARDLIB_API	__declspec(dllexport)
#else
#define CCARDLIB_CLASS	__declspec(dllimport)
#define CCARDLIB_API	__declspec(dllimport)
#endif


#define	LINK_CARDLIB_DLL

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__1139F678_9740_4111_9E6F_15BCE64CD0ED__INCLUDED_)
