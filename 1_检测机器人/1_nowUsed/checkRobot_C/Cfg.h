#ifndef		__CFG_H__
#define		__CFG_H__

#include "tinicfg.h"

typedef struct tagCONFIG_INFO
{
	char	ip_s[16];			//sios ip
	char	syscode[6];			
	char	terminalno[6];		
	char	timeout[6];		
}CONFIG_INFO;
BEGIN_BIND_CFG( CONFIG_INFO, UpdateCfg, "config.ini" )
	BIND_CFG_SECTION( "TESTAPI" )
		BIND_CFG_STR( "SIOS_IP", ip_s, "127.0.0.1" )
		BIND_CFG_STR( "SYSCODE", syscode, "20" )
		BIND_CFG_STR( "terminalno", terminalno, "1" )
		BIND_CFG_STR( "timeout", timeout, "20" )
END_BIND_CFG

#endif __CFG_H__