#pragma once
#include <stdlib.h>
/*
					ʹ��˵��

typedef struct tagCONFIG_INFO
{
	char acc[6];
	char lpath[MAX_PATH];
}CONFIG_INFO;

BEGIN_BIND_CFG( CONFIG_INFO, UpdateCfg, "config.ini" )
	BIND_CFG_SECTION( "BANKDEF" )
		BIND_CFG_STR( "ACC", acc, "73210" )
	BIND_CFG_SECTION( "COMPARE" )
		BIND_CFG_STR( "LOCALPATH", lpath, "c:\\inetpub\\ftproot" )
END_BIND_CFG
*/

#define BEGIN_BIND_CFG( cfg_type, fun_name, file_name )				\
	template<class cfg_type>										\
	BOOL fun_name( cfg_type& cfg, BOOL bSave = TRUE )				\
	{																\
		char ini_fname[MAX_PATH];									\
		GetModuleFileName( NULL, ini_fname, MAX_PATH );				\
		char* psz = strrchr( ini_fname, '\\' );						\
		psz ++;														\
		strcpy( psz, file_name );									\
		LPCTSTR lpszSection;

#define BEGIN_BIND_CFG_DLL( cfg_type, fun_name, file_name, dllkey )	\
	template<class cfg_type>										\
	BOOL fun_name( cfg_type& cfg, BOOL bSave = TRUE )				\
	{																\
		char ini_fname[MAX_PATH];									\
		GetModuleFileName( 	GetModuleHandle( dllkey ), ini_fname, MAX_PATH );\
		char* psz = strrchr( ini_fname, '\\' );						\
		psz ++;														\
		strcpy( psz, file_name );									\
		LPCTSTR lpszSection;

#define BIND_CFG_SECTION( s ) lpszSection = s;

#define BIND_CFG_STR( key, s, d )									\
	if( bSave )														\
	{																\
		if( !WritePrivateProfileString(	lpszSection, key,			\
				cfg.##s, ini_fname ) )								\
			return FALSE;											\
	}																\
	else															\
	{																\
		memset( cfg.##s, 0, sizeof( cfg.##s ) );					\
		GetPrivateProfileString( lpszSection, key, d, cfg.##s,		\
				sizeof( cfg.##s ), ini_fname );						\
	}

#define BIND_CFG_STR_STD( key ) BIND_CFG_STR( #key, key, "" )

#define BIND_CFG_STR_D( key, d ) BIND_CFG_STR( #key, key, d )

#define BIND_CFG_INT( key, n, d )									\
	if( bSave )														\
	{																\
		char buf[20];												\
		if( !WritePrivateProfileString(	lpszSection, key,			\
		itoa( cfg.##n, buf, 10 ), ini_fname ) )						\
			return FALSE;											\
	}																\
	else															\
	{																\
		cfg.##n = GetPrivateProfileInt(lpszSection, key, d,			\
						ini_fname );								\
	}

#define BIND_CFG_INT_D( key, d ) BIND_CFG_INT( #key, key, d )

#define BIND_CFG_INT_STD( key ) BIND_CFG_INT( #key, key, 0 )

#define END_BIND_CFG	return TRUE;	}
