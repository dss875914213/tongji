#if !defined(AFX_EXTBOX_H__FC511861_349A_11D7_A06E_A2CB42178833__INCLUDED_)
#define AFX_EXTBOX_H__FC511861_349A_11D7_A06E_A2CB42178833__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif

// ----- framework stuff
#ifndef _MT
	#error CExtBox requires a multithreaded C run-time library (please check your project settings)
#endif

#include <windows.h>
#include <stdio.h>
#include <commctrl.h>									// animate ctrl
#include <ocidl.h>										// IPicture interface
#include <olectl.h>										// OleLoadPicture
#include <process.h>									// _beginthreadex
#include <shlobj.h>										// for pathpicker

#ifndef ASSERT
	#include <crtdbg.h>
	#define ASSERT(f) _ASSERTE((f))
#endif

#ifndef HIMETRIC_INCH
	#define HIMETRIC_INCH   2540				// HIMETRIC units per inch
#endif

// ----- CExtBox... dwNewStyle (set by user):

#define BOX_BUTTON_RIGHT				1			// places buttons on the right side of the box
#define BOX_BUTTON_FLAT					2			// flat style
#define BOX_BUTTON_SUNKEN				4			// sunken style
#define BOX_BUTTON_STATIC				8			// simple border
#define BOX_BUTTON_DOUBLE				16		// double border
#define BOX_BUTTON_NONE					32		// no buttons if uiMillSecTimeout > 0
#define BOX_BUTTON_NONENOESC		64		// like BOX_BUTTON_NONE but pressing Esc won't close the box
#define BOX_BUTTON_CONTEXT			128		// includes a question mark in the title bar
#define BOX_BUTTON_ALIGNR				256		// right-justifies the text in the button rectangle
#define BOX_BUTTON_ALIGNL				512		// left-justifies...
#define BOX_BUTTON_BELOW				1024	// places one button beneath the other
#define BOX_BUTTON_MULTILINE		2048	// wraps the button text to multiple lines (\n possible)
#define BOX_BUTTON_LAYER				4096	// draws a sunken frame around the buttons
#define BOX_WIZARDSTYLE					8192	// places buttons at the bottom and draws a line above
#define BOX_ICONFRAME						16384	// draws a sunken frame around the icon
#define BOX_ANIMATE							32768	// avi resource instead of an image
#define BOX_TEXT_BELOW					65536	// places the text below the image
#define BOX_HICON								131072// nImageResID is an icon-handle
// ----- valid flags for splash boxes:
#define BOX_BORDER_NONE					1			// splash box without a border
#define BOX_BORDER_THIN					2			// splash box with a thin border
#define BOX_BORDER_SLIDE				4			// slide-like splash border
#define BOX_BORDER_DOUBLESLIDE	8			// slide-like splash border (double strength)

// ----- misc (rarely used) flags (set by user):

#define BOX_MISC_NOAVITRANS			1			// show avi resource without transparency
#define BOX_MISC_NOCAPTION			2			// box without a caption
#define BOX_MISC_WACENTER				4			// centers the box in the workarea
#define BOX_MISC_FORCETEXT			8			// show text (instead of system message) when system is low on memory
#define BOX_MISC_TIMER					16		// internally used

// ----- other defines (internally used):

#define BOX_ID_TIMEOUT					64		// return value for timeouts
#define BOX_ID_CODEDEND					65		// return value if BOXINFO::bEndDialog was TRUE or BOXISVISIBLEFUNC returned TRUE
#define BOX_ID_ADDCTRL					101		// id of the additional control
#define BOX_ID_BFFBUTTON				102		// id of the BrowseForFolder button
#define BOX_TIMEOUT_EVENT				99		// timeout timer identifier
#define BOX_SPACE								2			// space between buttons (pixel)
#define	BOX_MARGIN							6			// space between border and buttons
#define	BOX_CTRL_SPACE					16		// default space between controls
#define	BOX_CTRL_MARGIN					11		// space between border and statics
#define	BOX_BUTT_DEFWIDTH				98		// default button width
#define	BOX_BUTT_DEFHEIGHT			24		// default button height (not optimal)
#define	BOX_BUTT_OPTHEIGHT			26		// optimal button height
#define	BOX_EDIT_HEIGHT					20		// height of the edit control
#define	BOX_CHECK_YMARGIN				10		// vertical margin between frame and first (last) checkbox
#define	BOX_CHECK_XMARGIN				10		// horizontal margin between frame and first (last) checkbox
#define	BOX_PATH_XBUTTON				26		// width of the BrowseForFolder button
#define	BOX_PATH_HEIGHT					20		// height of the pathpicker
#define	BOX_DEFSTRINGLENGTH			512		// default size for buffer (LoadString)
#define	BOX_DELIMITER						'|'		// delimiter (loading string resources)

#define WM_USER_BOXEDIT					WM_USER + 0x91 // wParam: 0, lParam: points to a BOXINFO structure
#define WM_USER_BOXINTERNAL			WM_USER + 0x92 // internally used:
#define	BOX_INT_THREADEND				1

#define BOX_3DX			(GetSystemMetrics(SM_CXEDGE))
#define BOX_3DY			(GetSystemMetrics(SM_CYEDGE))
#define BOX_BRDX		(GetSystemMetrics(SM_CXBORDER))
#define BOX_BRDY		(GetSystemMetrics(SM_CYBORDER))
#define BOX_FRMX		(GetSystemMetrics(SM_CXFIXEDFRAME))
#define BOX_FRMY		(GetSystemMetrics(SM_CYFIXEDFRAME))
#define BOX_ICNX		(GetSystemMetrics(SM_CXICON))
#define BOX_ICNY		(GetSystemMetrics(SM_CYICON))
#define BOX_CAPY		(GetSystemMetrics(SM_CYCAPTION))

#ifndef ISSET
	#define ISSET(var,f1)						((var & f1) == f1)
	#define ISSET2(var,f1,f2)				(ISSET(var,f1) || ISSET(var,f2))
	#define ISSET3(var,f1,f2,f3)		(ISSET(var,f1) || ISSET(var,f2) || ISSET(var,f3))
	#define ISSET4(var,f1,f2,f3,f4)	(ISSET(var,f1) || ISSET(var,f2) || ISSET(var,f3) || ISSET(var,f4))
#endif


class CBuffer
{
	LPTSTR m_buff;
public:
	CBuffer();
	CBuffer(int nSize);
	CBuffer(LPCTSTR lpsz);
	~CBuffer();

	void Alloc(int nSize);
	void Copy(LPCTSTR lpsz);
	void Clear() { if(m_buff) { delete m_buff; m_buff = NULL; } }
	void Load(LPCTSTR szText, int nSize, HINSTANCE hModule = NULL);
	int Load(HWND hWndText);
	LPTSTR GetBuffer() { return m_buff; }
	int GetBufferLength() { return (m_buff ? strlen(m_buff) : 0); }
};

class CResLoader
{
	HMODULE m_hInst;
	LPPICTURE m_pPicture;

	void ReleasePicture();
	BOOL LoadResData(const UINT &nResID, LPCTSTR szResType, HGLOBAL &hGlobal,	DWORD &dwSize);

public:
	CResLoader(const HMODULE &hInstance = NULL);
	virtual ~CResLoader();

	BOOL LoadAniCursor(const UINT &nResID, HANDLE &hOut);
	BOOL LoadPicture(const UINT &nResID, LPCTSTR szResType);
	BOOL GetPictureDim(HDC dc = NULL, long* plWidth = NULL,
		long* plHeight = NULL, const BOOL &bInPixel = TRUE);
	BOOL DrawPicture(const HDC &dc, RECT &rcDraw, RECT &rcClient);
	// ...
};


//////////////////////////////////////////////////////////////////////////////
// CAuxThunk (slightly modified):
// Copyright (c) 1997-2001 by Andrew Nosenko <andien@nozillium.com>,
// (c) 1997-1998 by Computer Multimedia System, Inc.,
// (c) 1998-2001 by Mead & Co Limited
//
// http://www.nozillium.com/atlaux/
// Version: 1.10.0021
//

#ifndef _M_IX86
  #pragma message("CAuxThunk is implemented for X86 only!")
#endif

#pragma pack(push, 1)

template <class T>
class CAuxThunk
{
	friend class CExtBox;		// mgesing: inserted

  BYTE    m_mov;          // mov ecx, %pThis
  DWORD   m_this;         //
  BYTE    m_jmp;          // jmp func
  DWORD   m_relproc;      // relative jmp
//public:									// mgesing: changed from public to private
  typedef void (T::*TMFP)();
  void InitThunk(TMFP method, const T* pThis)
  {
    union { DWORD func; TMFP method; } addr;
    addr.method = (TMFP)method;
    m_mov = 0xB9;
    m_this = (DWORD)pThis;
    m_jmp = 0xE9;
    m_relproc = addr.func - (DWORD)(this+1);
    FlushInstructionCache(GetCurrentProcess(), this, sizeof(*this));
  }
  FARPROC GetThunk() const {
    _ASSERTE(m_mov == 0xB9);
    return (FARPROC)this; }
};

#pragma pack(pop)
//
//////////////////////////////////////////////////////////////////////////////


class CExtBox : public CAuxThunk<CExtBox>
{
public:
	// available infos during private (user) messages or callback operations
	struct BOXINFO
	{
		DWORD dwObjectID;			// object identifier
		HWND hwndFirstButton;	// handle of the first "positive" button
		HWND hwndText;				// handle of the static text (if any)
		HWND hwndAnimate;			// handle of the animate control (if any)
		HWND hwndAddCtrl;			// handle of the additional control (if any)
		LPCTSTR lpszText;			// text of the edit control (if any)
		BOOL bEndDialog;			// to close the box immediately
	};

	// pBoxInfo --> points to various informations (see BOXINFO)
	// hEventClose --> event-handle, signaled when the user wants to close the box
	// return values: TRUE: close the box, FALSE: don't close it
	typedef BOOL (CALLBACK *BOXVISIBLEFUNC)(BOXINFO* pBoxInfo, HANDLE hEventClose);

private:
	struct SPLASHBOX
	{
		CResLoader Loader;
		RECT rcText;
		COLORREF crTextColor;
		UINT uDTflags;
		int nShadow;
		HFONT hFont;
	};
	struct EDITBOX
	{
		DWORD dwStyle;
		HWND hEdit;
		BOOL bEnableButt1, bNotify;
	};
	struct PROGRESSBOX
	{
		int nLower, nUpper, nBoxReturn;
		BOOL bSmooth, bEnableButt1, bFuncRet;
		HWND hProgress;
		COLORREF crProgress;
		BOXVISIBLEFUNC pVisFunc, pVisFuncCopy;
		HANDLE hThread, hEvent;
		UINT uThreadID;
		BOXINFO info;
		CRITICAL_SECTION csect;
	};
	struct COMBO_BOX
	{
		int* pSelected;
		DWORD* pData;
		int nDataSize, nHeight, nHeightTotal, nWidthList;
		HWND hCombo;
		BOOL bInput;
	};
	struct CHECK_BOX
	{
		int* pSelected;
		BOOL bFrame, bRadio, bMultiline;
		int cyItem, nSelSize;
		HWND* phItems;
	};
	struct PATHBOX
	{
		int nImg;
		HANDLE hImg;
		CBuffer buff;
		HWND hEdit, hButt;
		BOOL bValidate, bEnableButt1, bSlash;
	};
	struct LIST_BOX
	{
		int* pSelected;
		DWORD ExStyle;
		int nHeight, nSelSize;
		BOOL bSingleSel;
		HWND hList;
	};

	enum { MESSAGE = 0, INPUT, SPLASH, PROGRESS, COMBO, CHECK, PATHPICKER, LIST } m_BoxType;

	HHOOK					m_hHook;
	HINSTANCE			m_hInst, m_hComCtl;
	HWND					m_hBox, m_hOwner, m_hAni, m_hText, m_hButt1;
	DWORD					m_ExStyle, m_BsStyle, m_NewStyle, m_BoxExStyle, m_dwObjID, m_Misc;
	RECT					m_rcImg, m_rcLine;
	POINT					m_ptBox, m_ptText;
	HANDLE				m_hBoxImage;
	LPCTSTR				m_pCaption;
	CBuffer				m_Buff;
	UINT					m_nImgResID;
	int						m_ButtCount, m_n, m_nTmp;
	long					m_lPrevProc;			// for subclassing
	void*					m_pHeap;					// points to one of the structures above

  struct
  {
		HANDLE	hPic;
		UINT		uiPicType;
		RECT		rcDim;
		LPCTSTR	szText;
  } m_Butt[4];

	void CalcCoordinates();
	void HeapData(const BOOL &bNew = FALSE);
	void CreateCtrl(const int &nAddYBefore, const int &nAddYAfter);
	void CreateComCtrl(LPCTSTR szClassName, const int &nAddYBefore = 0,	const int &nAddYAfter = 0);
	void ChangeDlgCtrl(const int &nType, const HWND &hwnd);
	UINT LoadPic(const int &nResID, HANDLE &hOut, const UINT &fuLoad = 0);
	LRESULT HookProc(int nCode, WPARAM wParam, LPARAM lParam);
	LRESULT WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	static BOOL	CALLBACK ChildProc(HWND hwnd, LPARAM lParam);
	static UINT WINAPI ThreadFunc(void* pParam);
	
	inline void	_Rect(RECT* prc, const int &x1=0, const int &x2=0, const int &y1=0, const int &y2=0)
		{ prc->left = x1; prc->right = x2; prc->top = y1; prc->bottom = y2; }
	inline void	_Point(POINT* ppt, const int &x=0, const int &y=0)
		{ ppt->x = x; ppt->y = y; }
	inline void	_AddPoint(POINT* ppt, const int &x=0, const int &y=0)
		{ ppt->x += x; ppt->y += y; }

public:
	CExtBox(const HINSTANCE &hInstance = NULL);
	virtual ~CExtBox();

	int	Message(const HWND &hOwner, LPCTSTR szText, LPCTSTR szCaption,
		const DWORD &dwStyle, const DWORD &dwNewStyle = 0, const UINT &nImageResID = 0,
		const int &nMillSecTimeout = 0, const DWORD &dwObjectID = 0);

	void SetButtons(const BOOL &bShowsText = TRUE, int *pWidthHeight = NULL,
		const int &nSize1 = 8, LPCTSTR *pTextImage = NULL, const int &nSize2 = 4);

	void SetEdit(LPCTSTR szDefEdit = NULL, const BOOL &bNotify = FALSE,
		const BOOL &bEnableFirstButton = TRUE, const DWORD &dwEditStyle = ES_AUTOHSCROLL);

	void SetSplash(LPCTSTR szResType, const int &x=0, const int &y=0, const int &cx=0,
		const int &cy=0, LPCTSTR szFontFace=NULL, const int &nPtFontHeight=10,
		const BOOL &bBold=FALSE, const COLORREF &crTextColor=0,
		const int &nShadowOffset=0,const UINT &uDTflags = DT_LEFT | DT_WORDBREAK);

	void SetProgress(BOXVISIBLEFUNC VisibilityFunc, const int &nLower = 0,
		const int &nUpper = 100, const BOOL &bSmooth = FALSE, const int &nHeight = 14,
		const COLORREF &crProgress = RGB(0,0,128), const BOOL &bEnableFirstButton = TRUE);

	void SetCombo(int* pSelected, LPCTSTR szItemText, DWORD *pData = NULL,
		const int &nDataSize = 0, const BOOL &bInput = FALSE,	const int &nHeight = 14,
		const int &nHeightTotal = 120, const int &nWidthList = 0,	const int &nItemTextMax = 1024);

	void SetCheck(int* pSelected, const int &nSelectedSize, LPCTSTR szItemText,
		const BOOL &bRadio = FALSE, const BOOL &bFrame = TRUE, const BOOL &bMultiline = FALSE,
		const int &cyItem = 22, const int &nItemTextMax = 1024);

	void SetPathPicker(LPCTSTR szDefPath = NULL, LPCTSTR szBffText = NULL,
		const int &nButtImg = 0, const BOOL &bSlashEnd = FALSE, const BOOL &bValidate = FALSE,
		const BOOL &bEnableFirstButton = TRUE);

	void SetList(int* pSelected, const int &nSelectedSize, LPCTSTR szItemText,
		const DWORD &dwExStyle = 0,	const BOOL &bSingleSelect = FALSE,
		const int &nListHeight = 80, const int &nItemTextMax = 1024);

	// returns whatever the user has entered
	LPCTSTR GetUserInput()
		{ return (m_Buff.GetBuffer() ? (LPCTSTR) m_Buff.GetBuffer() : "\0"); }

	// flags for miscellaneous (rarely used) stuff, 0: use default value
	void SetMiscFlags(const DWORD &dwNewMiscFlag = 0)
		{ m_Misc = dwNewMiscFlag; }

	// specifies the width and height of the static text, 0: use default values
	// use this procedure to increase the width of the box
	void SetStaticTextDim(const int &cx = 0, const int &cy = 0)
		{ m_ptText.x = cx; m_ptText.y = cy; }

	// overridable: called when the box is being activated
	// hwndBox --> handle of the box
	// x --> recommended horizontal position for a new window
	// y --> recommended vertical position for a new window
	// cx --> recommended width for a new window, call SetStaticTextDim to increase the width
	// pIncreaseHeight --> use this pointer to increase the height of the box
	// return values --> overrides should return TRUE
	virtual BOOL OnAddControl(const HWND &hwndBox, const int &x, const int &y,
		const int &cx, int* pIncreaseHeight) { return FALSE; }

	// overridable: called when the box is being destroyed
	// hwndBox --> handle of the box
	virtual void OnRemoveControl(const HWND &hwndBox) { }

	// overridable: called after message processing but before passing the message to the window procedure
	// hwndBox --> handle of the box
	// uMsg --> message identifier
	// wParam --> first message parameter
	// lParam --> second message parameter
	// return values --> TRUE: pass message to the window procedure, FALSE: don't pass it
	virtual BOOL OnDispatchMessage(const HWND &hwndBox, const UINT &uMsg,
		const WPARAM &wParam, const LPARAM &lParam) { return TRUE; }
};

#endif
