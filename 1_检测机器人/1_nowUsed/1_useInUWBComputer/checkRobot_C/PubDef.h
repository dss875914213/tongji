
#ifndef __PUBDEF_H__
#define __PUBDEF_H__







#define SendFilePath		"SendTemp"		//发送文件临时目录
#define RecvFilePath		"RecvTemp"		//接收文件临时目录
#define RecvPhotoPath		"Photo"			//接收相片文件目录

#define ControlFilePath		"ControlFile"		//接收控制文件目录

#define ConFileName			"Control.bin"	//控制文件名称


//定义各个配置项

#define		SIOS_SHM_NAME			"SIOS_SHM"




#define MAX_PKG_LEN				970					//每一包发送的最大长度
#define MAC_LEN					8					//MAC校验的长度






#define DOWN_CF_NOTICE		"85\x0"		/*通知UNIX生成控制文件*/




#define SIOS_WL_NAME			"SIOS_WHITELIST"
#define	PGM_SHM_NAME			"FRONT_SHM2"	//综合前置机白名单共享内存名称


//全局变量区
ULONG		g_lPrivateSign;				//私有签名计数
char		g_szWorkDir[MAX_PATH];		//应用程序路径
BOOL		g_bDllHasInit = FALSE;		//动态连接库初始化标志

BOOL		g_bshmFlag=FALSE;
char		g_sPinkey[20];


typedef int WINAPI MyCard_Beep(HANDLE,unsigned int mSecond);
typedef HANDLE WINAPI MyCard_Init(BYTE CardReaderType,int port,long Baud_Rate,BOOL SetCamyCode);//最后一个参数需要设置为2
typedef int WINAPI MyCard_Exit(HANDLE handle);

typedef int WINAPI MyCard_GetCardNoAnyTime(HANDLE,unsigned int *);
typedef int WINAPI MyCard_ChangeUseCardNumber(HANDLE handle , WORD UseCardNumber);
typedef int WINAPI MyCard_Consume(HANDLE handle ,int *nMoney,bool IsVerify);/*nMoney in tranmt [out]balance*/
typedef int WINAPI MyCard_ChangeCardPara(HANDLE handle , unsigned int Inputdata,BYTE changetype);

typedef void WINAPI MySetInitKey(unsigned char*  CamyCode,unsigned char*  DesKEY,unsigned char*  Companykey);

typedef int WINAPI MyCard_GetAccountMsg(HANDLE handle,  struct OpenAccount * lpAccountMsg);
typedef int WINAPI MyCard_ChangeMoney(HANDLE handle ,unsigned long Money,bool IsAddMoney);//IsAddMoney:标识此次修改余额是否为补助操作
typedef int WINAPI MyCard_GetCardNo(HANDLE handle, struct ReadCardInfo *lpCardInfo);

///////////////////////////第三方变量区//////////////////////
BOOL			g_bThirdApi = FALSE;
int				g_nThirdNode = 0;
unsigned short	g_nThirdSysCode = 0;
UCHAR			g_chCardKey[20];
char			g_szProxyIp[16];
short			g_sProxyPort = 8500;
HANDLE			g_hCardReader = NULL;
HMODULE			g_hCR_Dll=NULL;
UCHAR			g_chShamKey[8];

MyCard_Beep						*pCard_Beep;
MyCard_Init						*pCard_Init;
MyCard_Exit						*pCard_Exit;

MyCard_GetCardNoAnyTime			*pCard_FastGetCardNo = NULL;
MyCard_ChangeUseCardNumber		*pCard_ChgUsedCardNum = NULL;
MyCard_Consume					*pCard_Consume = NULL;
MyCard_ChangeCardPara			*pCard_ChgCardPara = NULL;
MySetInitKey					*pSetInitKey = NULL;

MyCard_GetAccountMsg			*pCard_GetAccMsg = NULL;
MyCard_ChangeMoney				*pCard_ChgMoney = NULL;
MyCard_GetCardNo				*pCard_GetCardNo = NULL;

//定义CardInterface的返回值
#define ERR_CR_OK				0		//成功
#define ERR_CR_FAIL				-1		//失败
#define ERR_CR_EXCEED_QUOTA		10		//超过消费限额
#define ERR_CR_LOW_BALAN		-10		//余额不足



#pragma pack(1)
typedef struct
{
	char	Name[31];
	char	Sex[2];
	char	DeptCode[19];
	long	CardId;
	long	Account;
	char	Sno[21];
	char	PID[3];
	char	Identity[21];
	char	IdentityId[13];
	long	CardBalance;
	char	ExpDate[9];
	long	SubSeq;
	long	IsOpenInSys;
}Third_AccMsg;
#pragma pack()

//定义DSQL没有查到记录的返回值
#define DSQL_NO_REC				4

#endif  //__PUBDEF_H__