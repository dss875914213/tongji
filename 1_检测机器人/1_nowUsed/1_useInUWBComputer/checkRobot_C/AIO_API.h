// AIO_API.h : main header file for the AIO_API DLL
//

#if !defined(AFX_AIO_API_H__BD1503C4_0E12_45CB_855F_3B8A41D7790D__INCLUDED_)
#define AFX_AIO_API_H__BD1503C4_0E12_45CB_855F_3B8A41D7790D__INCLUDED_
#include<windows.h>
#include<WinDef.h>

#include "resource.h"		// main symbols
//#include "precompilepsam.h"
//#include "whitelist.h"
#include "TransThird.h"
//#include "psam.h"
#include "PubDef.h"
//#include "tran.h"


//函数声明部分
#define EXTC extern "C"
/*函数名:G_GetAPIPath
**功能:取得API的路径
**输出参数:szPath－API的路径缓冲区，nBuffLen － 缓冲区的长度
**输入参数:无
**返回值:void
**创建日期：2003-07-08
*/
EXTC void WINAPI G_GetAPIPath(char *szPath , WORD nBuffLen);




/*
**函数名称：NC_DownPhotoFile
**函数功能：下载相片文件
**输入参数：IP-SIOS的IP地址, port-sios 的端口号,
**			IDNo-身份序号，PhotoFn-相片文件的名称
**输出参数: 无
**返回值:	大于0表示上送的文件的大小，小于0表示失败
**创建时间：2003-09-08
*/
EXTC int  WINAPI NC_DownPhotoFile(char *IP , short Port , 
								   char * IDNo , 
								   char *PhotoFn, 
								   short TimeOut=10);

EXTC int WINAPI NC_DownControlFile(char * IP,short port,short timeOut = 10);

/*
**函数名称：TA_Init
**函数功能：设置动态库以第三方方式运行
**参数说明：IP-SIOS的IP地址, port-sios 的端口号 
**			SysCode-系统代码,ProxyOffline-代理服务是否脱机
**			MaxJnl-最大流水号
**返回值：TRUE/FALSE
**创建时间：2003-08-21
**修改时间：2004-03-10
**修改内容：去掉参数CardKey-卡片密钥
**			改名TA_Init
*/
EXTC BOOL  WINAPI TA_Init(char *IP , short port , unsigned short SysCode, 
						  unsigned short TerminalNo, bool *ProxyOffline, ULONG *MaxJnl);
/*
**函数名称：TA_Init2
**函数功能：设置动态库以第三方方式运行
**参数说明：IP-SIOS的IP地址, port-sios 的端口号 
**			SysCode-系统代码,ProxyOffline-代理服务是否脱机
**			MaxJnl-最大流水号
**			signonPWD-最大长度为16的签到密码
**返回值：TRUE/FALSE
**创建时间：2007-05-16
**修改内容：同TA_Init,增加一个参数，登录密码
*/
EXTC BOOL  WINAPI TA_Init2(char *IP, short port, unsigned short SysCode, unsigned short TerminalNo,
						   bool *ProxyOffline, ULONG *MaxJnl, char* signonPWD);
/*
**函数名称：TA_Init3
**函数功能：设置动态库以第三方方式运行
**参数说明：IP-SIOS的IP地址, port-sios 的端口号 
**			SysCode-系统代码,ProxyOffline-代理服务是否脱机
**			MaxJnl-最大流水号
**			signonPWD-最大长度为16的签到密码
**返回值：TRUE/FALSE
**创建时间：2007-05-16
**修改内容：同TA_Init,增加一个参数，登录密码
*/
EXTC int  WINAPI TA_Init3(char *IP, short port, unsigned short SysCode, unsigned short TerminalNo,
						   bool *ProxyOffline, ULONG *MaxJnl, char* signonPWD);


/*
**函数名称：TA_CRInit
**函数功能：初始化读卡器
**参数列表：CardReaderType－输入参数，读卡器类型，0－usb读卡器，1－串口读卡器。
**			port－输入参数，端口号，
**			Baud_Rate－输入参数，波特率，
**返回值:	见返回值列表
**创建时间：2004-03-10
*/
EXTC int _stdcall TA_CRInit(char CardReaderType,int port,long Baud_Rate);

/*
**函数名称：TA_CRClose
**函数功能：关闭读卡器
**参数列表：void
**返回值:	true/false
**创建时间：2004-03-10
*/
EXTC BOOL _stdcall TA_CRClose(void);

/*
**函数名称：TA_FastReadCard
**函数功能：快速读卡号，适用于检验是否有卡靠近读卡反应区
**			如果只需要快速读取卡号，则不需要初始化动态库(TA_Init())
**参数列表：CardNo－输出参数，从卡片中读出的卡号
**返回值:	见返回值列表Errormsg.h
**创建时间：2004-03-10
*/
EXTC int _stdcall TA_FastGetCardNo(unsigned int *CardNo);


/*
**函数名称：TA_CRBeep
**函数功能：读卡器峰鸣
**参数列表：BeepSecond－输入参数，峰鸣的毫秒数。
**返回值:	见返回值列表Errormsg.h
**创建时间：2004-03-10
*/
EXTC int _stdcall TA_CRBeep(unsigned int BeepMSecond);

/*
**函数名称：TA_ReadCardSimple
**函数功能：简单读卡信息，不检验白名单。
**参数列表：pAccMsg－输出参数，从卡片中读出的卡片信息。
**返回值:	见返回值列表Errormsg.h
**创建时间：2004-03-10
*/
EXTC int _stdcall TA_ReadCardSimple(AccountMsg * pAccMsg);




/*
**函数名称：TA_CheckWL
**函数功能：第三方根据帐号和卡号检查白名单。
**参数列表：AccountNo－输入参数，需要验证的帐号
**			CardNo－输入参数，卡号
**			CheckID－输入参数，是否检验身份开通关闭状态
**返回值:	见返回值列表Errormsg.h
**创建时间：2004-03-10
*/
EXTC int _stdcall TA_CheckWL (unsigned int AccountNo , unsigned int CardNo , bool CheckID=true);


/*
**函数名称：TA_ReadCard
**函数功能：读卡信息。读出卡信息并检验白名单，判断卡片的有效性。
**参数列表：pAccMsg－帐户信息包,如果需要请求补助时必须填写pAccMsg->TerminalNo
**			CheckID－输入参数，是否检验身份开通关闭状态
**			CheckSub－输入参数，是否提取补助
**返回值:	见返回值列表Errormsg.h
**创建时间：2004-03-10
*/
EXTC int _stdcall TA_ReadCard(AccountMsg *pAccMsg,bool CheckID=true ,bool CheckSub= false);


/*
**函数名称：TA_CardOpen
**函数功能：开通
**参数列表：pCardOper－第三方操作的整体数据包，在这里需要填写的参数是帐号和经手人。
**			pCardOper->RetCode是后台交易的返回值
**			TimeOut － 输入参数，超时时间（秒），缺省值为10秒。
**返回值:	见返回值列表Errormsg.h
**创建时间：2004-03-11
*/
EXTC int _stdcall TA_CardOpen(CardOperating *pCardOper, short TimeOut = 10);

/*
**函数名称：TA_CardClose
**函数功能：关闭
**参数列表：pCardOper－第三方操作的整体数据包，在这里需要填写的参数是帐号和操作员。
**			pCardOper->RetCode是后台交易的返回值
**			TimeOut － 输入参数，超时时间（秒），缺省值为10秒。
**返回值:	见返回值列表Errormsg.h
**创建时间：2004-03-11
*/
EXTC int _stdcall TA_CardClose(CardOperating *pCardOper, short TimeOut=10);

/*
**函数名称：TA_CardLost
**函数功能：挂失
**参数列表：pCardOper－第三方操作的整体数据包，在这里需要填写的参数是帐号和操作员。
**			pCardOper->RetCode是后台交易的返回值
**			TimeOut － 输入参数，超时时间（秒），缺省值为10秒。
**返回值:	见返回值列表Errormsg.h
**创建时间：2004-03-11
*/
EXTC int _stdcall TA_CardLost(CardOperating *pCardOper, short TimeOut=10);

/*
**函数名称：TA_Consume
**函数功能：卡片消费(可以脱机)
**参数列表：pCardCons－第三方操作的整体数据包,要求必须填入卡号
**			pCardCons->RetCode是后台交易的返回值
**			IsVerfy－是否验证累计消费额，如果超过累计消费额，则需要输入消费密码。
**			TimeOut － 输入参数，超时时间（秒），缺省值为10秒。
**返回值:	见返回值列表Errormsg.h
**创建时间：2004-03-11
*/
EXTC int _stdcall TA_Consume(CardConsume *pCardCons, bool IsVerfy, short TimeOut=10);


/*
**函数名称：TA_Refund
**函数功能：卡片退费(联机交易)
**参数列表：pCardCons－第三方操作的整体数据包,要求必须填入卡号
**			pCardCons->RetCode是后台交易的返回值
**			TimeOut － 输入参数，超时时间（秒），缺省值为10秒。
**返回值:	见返回值列表Errormsg.h
**创建时间：2004-03-15
*/
EXTC int _stdcall TA_Refund(CardConsume *pCardCons , short TimeOut=10);



/*
**函数名称：TA_InqAccEx
**函数功能：根据帐号/卡号/学工号/证件号精确查询帐户信息
**参数列表：pAccMsg－第三方帐户信息的整体数据包
**			(需要填写卡号或者是帐号或者是学工号或者是证件号)。
**			pAccMsg->RetCode- 输出参数 , 为后台处理的返回值
**			TimeOut － 输入参数，超时时间（秒），缺省值为10秒。
**返回值:	见返回值列表Errormsg.h
**创建时间：2004-03-15
*/
EXTC int _stdcall TA_InqAcc(AccountMsg * pAccMsg, short TimeOut = 10);

/*
**函数名称：TA_InqAccEx
**函数功能：根据帐号/卡号/学工号/证件号精确查询帐户信息及其扩展信息
**参数列表：pAccMsg－第三方帐户信息的整体数据包
**			(需要填写卡号或者是帐号或者是学工号或者是证件号)。
**			pAccMsg->RetCode- 输出参数 , 为后台处理的返回值
**			TimeOut － 输入参数，超时时间（秒），缺省值为10秒。
**返回值:	见返回值列表Errormsg.h
**创建时间：2004-07-25
*/
EXTC int _stdcall TA_InqAccEx(AccountMsgEx * pAccMsg, short TimeOut = 10);



/*
**函数名称：TA_HazyInqAcc
**函数功能：模糊查询，查询条件(Name,DeptCode,SexNo,StudentNo,PID )
**参数列表：DeptCode－输入参数，部门代码，最少19个字节。
**			查询的文件放到RecvTemp目录下，文件名写入到FileName中
**			FileName－输出参数，返回的文件名称，最少64个字节
**			RecNum-输出参数，查询到的记录数目
**			TimeOut － 输入参数，超时时间（秒），缺省值为10秒。
**返回值:	见返回值列表Errormsg.h
**创建时间：2004-03-15
*/
EXTC int _stdcall TA_HazyInqAcc(AccountMsg *pAccMsg, int *RecNum , char *FileName,short TimeOut = 10);

/*
**函数名称：TA_HazyInqAccEx
**函数功能：扩展模糊查询，查询条件(Name,DeptCode,SexNo,StudentNo,PID )
**参数列表：DeptCode－输入参数，部门代码，最少19个字节。
**			查询的文件放到RecvTemp目录下，文件名写入到FileName中
**			FileName－输出参数，返回的文件名称，最少64个字节
**			RecNum-输出参数，查询到的记录数目
**			TimeOut － 输入参数，超时时间（秒），缺省值为10秒。
**返回值:	见返回值列表Errormsg.h
**创建时间：2004-07-25
*/
EXTC int _stdcall TA_HazyInqAccEx(AccountMsg *pAccMsg, int *RecNum , char *FileName,short TimeOut = 10);

/*
**函数名称：TA_InqAccByDeptXZ
**函数功能：根据部门和学制模糊查询帐户信息,
**			查询的文件放到RecvTemp目录下，文件名写入到FileName中
**参数列表：DeptCode－输入参数，部门代码
**			XZ-输入参数,学制*10,如果为4学年,则需要输入40,
**			FileName－输出参数，返回的文件名称，最少64个字节
**			RecNum-输出参数，查询到的记录数目
**			TimeOut － 输入参数，超时时间（秒），缺省值为10秒。
**返回值:	见返回值列表Errormsg.h
**创建时间：2004-09-17
*/
EXTC int _stdcall TA_InqAccByDeptXZ(char *DeptCode, long XZ,int *RecNum , char *FileName , short TimeOut=10);


/*
**函数名称：TA_InqAccByClass
**函数功能：根据班级名称和身份代码模糊查询部门信息,
**			查询的文件放到RecvTemp目录下，文件名写入到FileName中
**参数列表：ClassName－输入参数，班级名称(如01级或者2001级,具体根据部门名称定)
**			PID-输入参数,身份代码
**			FileName－输出参数，返回的文件名称，最少64个字节
**			RecNum-输出参数，查询到的记录数目
**			TimeOut － 输入参数，超时时间（秒），缺省值为10秒。
**返回值:	见返回值列表Errormsg.h
**创建时间：2004-08-25
*/
EXTC int _stdcall TA_InqAccByClass(char *ClassName, char * PID,int *RecNum , char *FileName , short TimeOut=10);

/*
**函数名称：TA_InqTranFlow
**函数功能：交易流水查询。
**参数列表：pInqTranFlow－输入参数,可以根据持卡人帐号、商户帐号、
**			终端号码组合查询当天的或者历史的交易流水。
**			查询的文件放到RecvTemp目录下，文件名写入到pInqTranFlow->FileName中。	
**			pInqTranFlow->RecNum-输出参数，查询到的记录数目
**			TimeOut － 输入参数，超时时间（秒），缺省值为10秒。
**返回值:	见返回值列表Errormsg.h
**创建时间：2004-03-15
*/
EXTC int _stdcall TA_InqTranFlow(InqTranFlow *pInqTranFlow, short TimeOut = 10);


/*
**函数名称：TA_InqOpenFlow
**函数功能：开通流水查询。
**参数列表：pInqOpenFlow－输入参数,可以根据持卡人帐号、商户帐号、
**			终端号码组合查询当天的或者历史的交易流水。
**			查询的文件放到RecvTemp目录下，文件名写入到pInqOpenFlow->FileName中。	
**			pInqOpenFlow->RecNum-输出参数，查询到的记录数目
**			TimeOut － 输入参数，超时时间（秒），缺省值为10秒。
**返回值:	见返回值列表Errormsg.h
**创建时间：2004-03-15
*/
EXTC int _stdcall TA_InqOpenFlow(InqOpenFlow *pInqOpenFlow, short TimeOut = 10);


/*
**函数名称：TA_Charge
**函数功能：卡片收费
**参数列表：pCardCharg－第三方操作的整体数据包,要求必须填入卡号
**			pCardCharg->RetCode是后台交易的返回值
**			IsVerfy－是否验证累计消费额，如果超过累计消费额，则需要输入消费密码。
**			TimeOut － 输入参数，超时时间（秒），缺省值为10秒。
**返回值:	见返回值列表Errormsg.h
**创建时间：2004-03-11
*/
EXTC int _stdcall TA_Charge(CardCharge *pCardCharg, bool IsVerfy, short TimeOut=10);

/*函数名:TA_DownControlFile
 *功能:下载控制文件，控制文件的目录是\ControlFile
 *输入参数:timeout－超时时间
 *输出参数:无
 *返回值:大于0表示下载的文件的大小，小于0表示失败
 */
EXTC int _stdcall TA_DownControlFile(short timeOut=10);



/*
**函数名称：TA_DownPhotoFile
**函数功能：下载相片文件
**输入参数：IDNo-身份序号，PhotoFn-相片文件的名称
**输出参数: 无
**返回值:	大于0表示上送的文件的大小，小于0表示失败
**创建时间：2004-03-19
*/
EXTC int  _stdcall TA_DownPhotoFile(char * IDNo , 
								   char *PhotoFn, 
								   short TimeOut=10);

/*
 *函数名称：TA_Dsql_QFile
 *函数功能：第三方DSQL查询(返回二进制文件或者文本文件)
 *参数列表：Dsql ：sql语句；fn：返回的文件名
 *			fillGap:分割符 (如果是'\0'返回二进制文件，
 *			其他返回以FillGap分隔的文本文件)；
 *			nblocknum：返回的记录数目；
 *			RetCode：Dsql查询的返回值
 *			timeOut－超时时间
 *返回值表：
 */
EXTC int _stdcall TA_Dsql_QFile(const char *Dsql , 
								char fillGap ,
								char *fn , 
								int * nBlockNum , 
								int	* RetCode ,
								short timeOut=10);

/*
 *函数名称：TA_Dsql_QRecord
 *函数功能：第三方DSQL精确查询(返回一条二进制记录或者文本记录, size < 1024)
 *参数列表：Dsql：sql语句；sBlock：返回记录缓冲区(size = 1024)
 *			fillGap:分割符 (如果是'\0'返回二进制文件，
 *			其他返回以FillGap分隔的文本文件)；
 *			nBlockSize：输入输出参数，输入－缓冲区的的大小，输出－返回的记录大小
 *			RetCode：Dsql查询的返回值
 *			timeOut－超时时间
 *返回值表：
 *备注：
 */
EXTC int _stdcall TA_Dsql_QRecord(	const char *Dsql , 
									char fillGap ,
									char * sBlock , 
									int * nBlockSize , 
									int	* RetCode ,
									short timeOut=10);

/*
**函数名:TA_BussComm
**功能:第三方向sios发送并接收加密和校验的数据包
**输入参数:sendBuf-要分解的数据;sendLen-要分解数据的长度,priority-优先级
**			timeout－通讯超时,缺省是10秒
**			recvLen-分配的缓冲区长度
**输出参数:recvBuf-解包完成后的数据;recvLen-解包后的数据长度
**返回值:参见返回值列表
**创建时间：2004-03-26
**修改记录：
*/
EXTC int WINAPI TA_BussComm(char* sendBuf,
							int sendLen,
							char * recvBuf,
							int *recvLen,
							char priority,
							short timeOut=10);

/*函数名:TA_DownLoadFile
 *功能:下载文件，返回文件的目录是\RecvTemp
 *输入参数:fn-文件名称，timeout－超时时间
 *输出参数:无
 *返回值:大于0表示下载的文件的大小，小于0表示失败
 */
EXTC int _stdcall TA_DownLoadFile(char *fn , short timeOut=5);


/*
**函数名称：TA_PermitID
**函数功能：启用身份功能
**参数列表：pCardOper－第三方操作的整体数据包，在这里需要填写的参数是帐号和操作员。
**			pCardOper->RetCode是后台交易的返回值
**			TimeOut － 输入参数，超时时间（秒），缺省值为10秒。
**返回值:	见返回值列表Errormsg.h
**创建时间：2004-07-15
*/
EXTC int _stdcall TA_PermitID(CardOperating *pCardOper, short TimeOut=10);

/*
**函数名称：TA_ProhibitID
**函数功能：禁用身份功能
**参数列表：pCardOper－第三方操作的整体数据包，在这里需要填写的参数是帐号和操作员。
**			pCardOper->RetCode是后台交易的返回值
**			TimeOut － 输入参数，超时时间（秒），缺省值为10秒。
**返回值:	见返回值列表Errormsg.h
**创建时间：2004-07-15
*/
EXTC int _stdcall TA_ProhibitID(CardOperating *pCardOper, short TimeOut=10);


/*函数名:TA_UpLoadFile
 *功能:第三方上传文件，上传文件的目录是\SendTemp
 *输入参数:fn-文件名称，timeout－超时时间
 *输出参数:无
 *返回值:大于0表示下载的文件的大小，小于0表示失败
 */
EXTC int _stdcall TA_UpLoadFile(char *fn , short timeOut=10);




/*函数名:TA_OpenEncard
 *功能:第三方打开加密卡
 *输入参数:无
 *输出参数:无
 *返回值:TRUE/FALSE
 *创建时间:2004-08-18
 */
EXTC BOOL _stdcall TA_OpenEncard();

/*函数名:TA_GetSectKeyNet
 *功能:通过网络取得卡片扇区的密钥
 *输入参数:CardID:卡片的序列号
 *输出参数:sectkey:卡片的扇区密钥
 *返回值:见返回值列表errormsg.h
 *备注:不需要在本机安装加密卡,前提是执行TA_Init函数成功
 *创建时间:2004-08-18
 */
EXTC BOOL  WINAPI TA_GetSectKeyNet(ULONG CardID, char * sectkey);

/*函数名:TA_GetSectKey
 *功能:通过API取得卡片扇区的密钥
 *输入参数:CardID:卡片的序列号
 *输出参数:sectkey:卡片的扇区密钥
 *返回值:TRUE/FALSE
 *备注:需要在本机安装加密卡,并且首先执行TA_OpenEncard函数成功
 *创建时间:2004-08-18
 */
EXTC BOOL  WINAPI TA_GetSectKey(ULONG CardID, char * sectkey);

/*函数名:TA_ExtractConFile
 *功能:导出控制文件
 *输入参数:readrec-读取的记录序号,fn:输出的文件名称
 *输出参数:无
 *返回值:TRUE/FALSE
 *创建时间:2004-08-26
 */
EXTC BOOL WINAPI TA_ExtractConFile(const int readrec , const char * fn);

/*
 *函数名:TA_CheckQpwd
 *功能:根据账号验证查询密码
 *输入参数:accountno-持卡人账号,qpwd:持卡人查询密码
 *输出参数:无
 *返回值:0-验证成功 / 1-验证失败 / -1-查询交易失败
 *备注:为武汉理工网站增加
 *创建时间:2004-10-14
 */
EXTC int _stdcall TA_CheckQpwd(unsigned int accountno , char * qpwd);


/*
**函数名称：TA_CheckConsumePWD
**函数功能：验证消费密码
**参数列表：pwd-消费密码
**返回值:	见返回值列表Errormsg.h
**创建时间：2009-06-26
*/
EXTC int _stdcall TA_CheckConsumePWD(unsigned char pwd[7]);



/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AIO_API_H__BD1503C4_0E12_45CB_855F_3B8A41D7790D__INCLUDED_)