#include <iostream>
#include <Windows.h>

#define BUF_SIZE 10

using namespace std;
int sendPhysicalID(unsigned int physicalID)
{
	char buf_msg[] = "4015414414";
	sprintf_s(buf_msg, "%10u", physicalID);
	HANDLE h_pipe;
	DWORD num_rcv; //实际接收到的字节数
	cout << "Try to connect named pipe...\n";
	//连接命名管道
	if (::WaitNamedPipe("\\\\.\\pipe\\test_pipe", NMPWAIT_WAIT_FOREVER))
	{
		//打开指定命名管道
		h_pipe = ::CreateFile("\\\\.\\pipe\\test_pipe", GENERIC_WRITE, 0, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
		if (h_pipe == INVALID_HANDLE_VALUE)
		{
			cerr << "Failed to open the appointed named pipe!Error code: " << ::GetLastError() << "\n";
			::system("pause");
			return 1;
		}
		else
		{
			if (::WriteFile(h_pipe, buf_msg, BUF_SIZE, &num_rcv, nullptr))
			{
				cout << "Message sent successfully...\n";
			}
			else
			{
				cerr << "Failed to send message!Error code: " << ::GetLastError() << "\n";
				::CloseHandle(h_pipe);
				::system("pause");
				return 1;
			}
		}
		::CloseHandle(h_pipe);
	}
	return 0;
}