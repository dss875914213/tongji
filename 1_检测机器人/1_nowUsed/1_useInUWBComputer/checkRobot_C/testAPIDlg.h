// testAPIDlg.h : header file
//

#if !defined(AFX_TESTAPIDLG_H__3B7A4C53_3276_464B_A172_F32D718E53C6__INCLUDED_)
#define AFX_TESTAPIDLG_H__3B7A4C53_3276_464B_A172_F32D718E53C6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#pragma pack(1)

/*模糊查询帐户信息包*/
typedef struct
{
	char      		Name[31]; 			/*姓名四个汉字*/
	char      		SexNo[2]; 			/*性别*/
	char			DeptCode[19];		/*部门代码*/
	unsigned int	CardNo; 			/*卡号*/
	unsigned int	AccountNo; 		/*帐号*/
	char			StudentCode[21];	/*学号*/
	char			IDCard[21]; 		/*身份证号*/
	char			PID[3];			/*身份代码*/
	char			IDNo[13]; 			/*身份序号*/
	int				Balance; 			/*现余额*/
	char			ExpireDate[7];		/*账户截止日期*/
	unsigned int	SubSeq;			/*补助戳*/
	char			Flag[16];			/*卡片状态*/
}HazyInqAccMsg;


/*模糊查询电子帐户返回文件的数据格式*/
typedef struct
{
	char      		Name[31]; 			/*姓名四个汉字*/
	char      		SexNo[2]; 			/*性别*/
	char			DeptCode[19];		/*部门代码*/
	unsigned int	CardNo; 			/*卡号*/
	unsigned int	AccountNo; 			/*帐号*/
	char			StudentCode[21];	/*学号*/
	char			IDCard[21]; 		/*身份证号*/
	char			PID[3];				/*身份代码*/
	char			IDNo[13]; 			/*身份序号*/
	int				Balance; 			/*现余额*/
	char			ExpireDate[7];		/*账户截止日期*/
	unsigned int	SubSeq;				/*补助戳*/
	char			Flag[16];			/*卡片状态*/
	char			AccType[4];			/*电子账户类型*/
	long			AccAmt;				/*电子账户余额*/
	char			SchCode_E[3];		/*电子账户的核算单位*/	
}HazyInqAccMsg_E;


#pragma pack()


/////////////////////////////////////////////////////////////////////////////
// CTestAPIDlg dialog
class CTestAPIDlg : public CDialog
{
// Construction
public:
	CTestAPIDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CTestAPIDlg)
	enum { IDD = IDD_TESTAPI_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestAPIDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CTestAPIDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnSoftencryptcard();
	afx_msg void OnThieddsql();
	afx_msg void OnDowncontr();
	afx_msg void OnThirddownfile();
	afx_msg void OnDownphoto();
	afx_msg void OnThirdcrinit();
	afx_msg void OnThirdgetcardid();
	afx_msg void OnThirdreadcardsim();
	afx_msg void OnThirdreadcard();
	afx_msg void OnThirdcrclose();
	afx_msg void OnThirdopen();
	afx_msg void OnThirdclose();
	afx_msg void OnThirdlost();
	afx_msg void OnThirdconsume();
	afx_msg void OnThirdrefund();
	afx_msg void OnThirdinqacc();
	afx_msg void OnThirdhazyinqacc();
	afx_msg void OnThirdinqtranflow();
	afx_msg void OnThirdinqopenflow();
	afx_msg void OnThirdcharge();
	afx_msg void OnThirdbeep();
	afx_msg void OnCardloss();
	virtual void OnCancel();
	afx_msg void OnDumpcontrolfile();
	afx_msg void OnThirdinqacc2();
	afx_msg void OnThirdinqallacc();
	afx_msg void OnSoftencryptcard2();
	afx_msg void OnGetthirdRongliang();
	afx_msg void OnVerifypwd();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTAPIDLG_H__3B7A4C53_3276_464B_A172_F32D718E53C6__INCLUDED_)
