//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by testAPI.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TESTAPI_DIALOG              102
#define ID_BOX1                         116
#define IDR_MAINFRAME                   128
#define IDC_BUTTON1                     1000
#define IDC_BUTTON2                     1001
#define IDC_BUTTON3                     1002
#define IDC_IPADDRESS1                  1002
#define IDC_BUTTON4                     1003
#define IDC_EDIT1                       1003
#define IDC_BUTTON5                     1004
#define IDC_EDIT2                       1004
#define IDC_BUTTON6                     1005
#define IDC_EDIT3                       1005
#define IDC_BUTTON7                     1006
#define IDC_IDNO                        1006
#define IDC_BUTTON8                     1007
#define IDC_DownControlFile             1008
#define IDC_GETCFG                      1009
#define IDC_CARDLOSS                    1010
#define IDC_CARDUNLOSS                  1011
#define IDC_CARDCHGQUERYPIN             1012
#define IDC_GETMAXJN                    1013
#define IDC_SOFTENCRYPTCARD             1014
#define IDC_THIEDDSQL                   1015
#define IDC_DOWNCONTR                   1016
#define IDC_ENCRYPTPWD                  1017
#define IDC_DOWNCONTR2                  1018
#define IDC_THIRDDOWNFILE               1019
#define IDC_THIRDDOWNFILE2              1020
#define IDC_DOWNPHOTO                   1021
#define IDC_TRANSFER                    1022
#define IDC_GETTHIRD_RONGLIANG          1022
#define IDC_WL_GETACC                   1023
#define IDC_VERIFYPWD                   1023
#define IDC_THIRDCRINIT                 1024
#define IDC_THIRDGETCARDID              1025
#define IDC_THIRDREADCARDSIM            1026
#define IDC_THIRDREADCARD               1027
#define IDC_THIRDCRCLOSE                1028
#define IDC_THIRDOPEN                   1029
#define IDC_THIRDCLOSE                  1030
#define IDC_THIRDLOST                   1031
#define IDC_THIRDCONSUME                1032
#define IDC_THIRDREFUND                 1033
#define IDC_THIRDINQACC                 1034
#define IDC_THIRDHAZYINQACC             1035
#define IDC_THIRDINQTRANFLOW            1036
#define IDC_THIRDINQOPENFLOW            1037
#define IDC_THIRDCHARGE                 1038
#define IDC_THIRDBEEP                   1039
#define IDC_DUMPCONTROLFILE             1040
#define IDC_THIRDINQACC2                1041
#define IDC_THIRDINQALLACC              1042
#define IDC_SOFTENCRYPTCARD2            1043

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1004
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
