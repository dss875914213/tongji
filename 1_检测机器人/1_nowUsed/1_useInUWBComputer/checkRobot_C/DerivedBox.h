#if !defined(AFX_DERIVEDBOX_H__EBD3BCC1_95F4_11D7_A771_EE2C5241AF2C__INCLUDED_)
#define AFX_DERIVEDBOX_H__EBD3BCC1_95F4_11D7_A771_EE2C5241AF2C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif

#include "ExtBox.h"

class DerivedBox : public CExtBox
{
	HINSTANCE m_hInst;
	HWND m_hTree, m_hStatus;
	HIMAGELIST m_il;
	LPCTSTR m_szData[5], m_szText[5];
	int m_nSelected;

	virtual BOOL OnAddControl(const HWND &hwndBox, const int &x, const int &y,
		const int &cx, int* pIncreaseHeight);

	virtual void OnRemoveControl(const HWND &hwndBox);

	virtual BOOL OnDispatchMessage(const HWND &hwndBox, const UINT &uMsg,
		const WPARAM &wParam, const LPARAM &lParam);

public:
	DerivedBox(const HINSTANCE &hInstance = NULL);
	virtual ~DerivedBox();

	int ShowTreeBox(const HWND &hOwner, LPCTSTR szText, LPCTSTR szCaption);

	LPCTSTR GetSelectedData()
		{ return (m_nSelected >= 0 ? m_szData[m_nSelected] : "\0"); }
};

#endif
