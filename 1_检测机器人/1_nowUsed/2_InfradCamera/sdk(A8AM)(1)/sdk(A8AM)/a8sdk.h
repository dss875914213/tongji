#pragma once

#ifndef A8SDK_EXPORTS
#define EXPORT_API __declspec(dllimport)
#else
#define EXPORT_API __declspec(dllexport)
#endif

typedef struct
{
	int enable;
	int x;
	int y;
	int width;
	int height;
} area_pos;

typedef struct
{
	int enable;
	int x;
	int y;
} spot_pos;

typedef struct
{
	int enable;
	int sta_x;
	int sta_y;
	int end_x;
	int end_y;
} line_pos;

typedef struct
{
	area_pos area[6];
	spot_pos spot[6];
	line_pos line;
} image_pos;

typedef struct
{
	int enable;
	int max_temp;
	int max_temp_x;
	int max_temp_y;
	int min_temp;	
	int min_temp_x;
	int min_temp_y;	
	int ave_temp;
} area_temp;

typedef struct
{
	int enable;
	int temp;
} spot_temp;

typedef struct
{
	int enable;
	int max_temp;
	int max_temp_x;
	int max_temp_y;
	int min_temp;
	int min_temp_x;
	int min_temp_y;
	int ave_temp;
} line_temp;

typedef struct
{
	int max_temp;
	int max_temp_x;
	int max_temp_y;
	int min_temp;
	int min_temp_x;
	int min_temp_y;
} globa_temp;

typedef struct
{
	area_temp area[6];
	spot_temp spot[6];
	line_temp line;
	globa_temp globa;
} image_temp;

typedef struct
{
	int enable;
	char recv_addr[20];
	char send_addr[20];
	char send_pwd[20];
} email_server;

typedef struct
{
	int enable;
	char tftp_addr[20];
} tftp_server;

typedef struct
{
	int enable;
	char static_ip[20];
	char netmask[20];
	char gateway[20];
	char dns1[20];
	char dns2[20];
} network_eth;

typedef struct
{
	int method;
	int num;
	float emissivity;
	float airTemp;
	float targetTemp;
	float atmosTrans;
	float distance;
	float infraredTemp;
	float infraredRadia;
} envir_param;

typedef struct
{
	int method;
	int num;
	int active;
	int condition;
	int captrue;
	int disableCalib;
	int email;
	int digital;
	int ftp;
	float threshold;
	float hysteresis;
	int thresholeTime;
} alarm_param;

extern "C"
{
	EXPORT_API int sdk_initialize();
	EXPORT_API int sdk_destroy();

	EXPORT_API int sdk_tcp_pthread_init(const char* ip);
	EXPORT_API int sdk_tcp_pthread_destroy(const char* ip);
	EXPORT_API int sdk_temp_buf_get(char *data_buff, const char* ip);
	EXPORT_API int sdk_image_width_switch(char mode);

	EXPORT_API int sdk_search_device(char* device_list, int list_len); 

	EXPORT_API int sdk_shutter_correction(const char* ip);

	EXPORT_API int sdk_set_shutter_auto_correction(const char* ip, int time_sec);
	EXPORT_API int sdk_get_shutter_auto_correction(const char* ip, int* time_sec);

	EXPORT_API int sdk_set_color_plate(const char* ip, int color_plate);
	EXPORT_API int sdk_get_color_plate(const char* ip, int* color_plate);

	EXPORT_API int sdk_set_video_mirror(const char* ip, int mirror_mode);
	EXPORT_API int sdk_get_video_mirror(const char* ip, int* mirror_mode);

	EXPORT_API int sdk_set_video_mode(const char* ip, int video_mode);
	EXPORT_API int sdk_get_video_mode(const char* ip, int* video_mode);

	EXPORT_API int sdk_set_area_pos(const char* ip,int index, area_pos* area_data);
	EXPORT_API int sdk_get_area_pos(const char* ip,int index, area_pos* area_data);

	EXPORT_API int sdk_set_spot_pos(const char* ip,int index, spot_pos* spot_data);
	EXPORT_API int sdk_get_spot_pos(const char* ip,int index, spot_pos* spot_data);

	EXPORT_API int sdk_set_line_pos(const char* ip, line_pos* line_data);
	EXPORT_API int sdk_get_line_pos(const char* ip, line_pos* line_data);

	EXPORT_API int sdk_get_all_pos(const char* ip, image_pos* image_data);

	EXPORT_API int sdk_set_temp_range(const char* ip, int temp_mode);
	EXPORT_API int sdk_get_temp_range(const char* ip, int* temp_mode);

	EXPORT_API int sdk_set_video_isp_x_offset(const char* ip, int lr_offset);
	EXPORT_API int sdk_get_video_isp_x_offset(const char* ip, int* lr_offset);

	EXPORT_API int sdk_set_video_isp_y_offset(const char* ip, int tb_offset);
	EXPORT_API int sdk_get_video_isp_y_offset(const char* ip, int* tb_offset);

	EXPORT_API int sdk_set_video_isp_x_scale(const char* ip, int scale_x);
	EXPORT_API int sdk_get_video_isp_x_scale(const char* ip, int* scale_x);

	EXPORT_API int sdk_set_video_isp_y_scale(const char* ip, int scale_y);
	EXPORT_API int sdk_get_video_isp_y_scale(const char* ip, int* scale_y);

	EXPORT_API int sdk_set_led(const char* ip, int light_param);
	EXPORT_API int sdk_get_led(const char* ip, int* light_param);

	EXPORT_API int sdk_set_email_server(const char* ip, email_server* server_param);
	EXPORT_API int sdk_get_email_server(const char* ip, email_server* server_param);

	EXPORT_API int sdk_set_tftp_server(const char* ip, tftp_server* server_param);
	EXPORT_API int sdk_get_tftp_server(const char* ip, tftp_server* server_param);

	EXPORT_API int sdk_set_network_eth(const char* ip, network_eth* network_param);
	EXPORT_API int sdk_get_network_eth(const char* ip, network_eth* network_param);

	EXPORT_API int sdk_set_fusion_distance(const char* ip, int distance);
	EXPORT_API int sdk_get_fusion_distance(const char* ip, int* distance);

	EXPORT_API int sdk_set_envir_param(const char* ip, envir_param* envir_data);
	EXPORT_API int sdk_get_area_envir_param(const char* ip, int index, envir_param* envir_data);
	EXPORT_API int sdk_get_spot_envir_param(const char* ip, int index, envir_param* envir_data);
	EXPORT_API int sdk_get_line_envir_param(const char* ip, envir_param* envir_data);
	EXPORT_API int sdk_get_globa_envir_param(const char* ip, envir_param* envir_data);

	EXPORT_API int sdk_set_alarm_param(const char* ip, alarm_param*  alarm_data);	
	EXPORT_API int sdk_get_area_alarm_param(const char* ip, int index, alarm_param*  alarm_data);	
	EXPORT_API int sdk_get_spot_alarm_param(const char* ip, int index, alarm_param*  alarm_data);
	EXPORT_API int sdk_get_line_alarm_param(const char* ip, alarm_param*  alarm_data);

	EXPORT_API int sdk_get_area_temp_data(const char* ip,int index, area_temp* area_data);
	EXPORT_API int sdk_get_spot_temp_data(const char* ip, int index, spot_temp* spot_data);
	EXPORT_API int sdk_get_line_temp_data(const char* ip, line_temp* line_data);
	EXPORT_API int sdk_get_globa_temp_data(const char* ip, globa_temp* globa_data);
	EXPORT_API int sdk_get_all_temp_data(const char* ip, image_temp* all_data);


	EXPORT_API int sdk_power_reboot(const char* ip);
	EXPORT_API int sdk_param_recover(const char* ip);
	EXPORT_API int sdk_update(const char* ip);
	EXPORT_API int sdk_heartbeat(const char* ip);

}
