# 应变片测试电路:sunglasses:


|Author|0点51胜
|--|--|
|E-mail|dss875914213@163.com

[MAX1452文档](https://datasheets.maximintegrated.com/en/ds/MAX1452.pdf)

## 主要操作的东西:car:
    1. 接口寄存器  DHR;IEEA;ICRA;CRIL;IRSP;ALOC;ATIM;IEED;TEMP-Index;BitClock  
    2. 5个校准寄存器   CONFIG;ODAC;OTCDAC;FSODAC;FSOTCDAC  
    3. EEPROM

### 5个校准寄存器:airplane:
    当UNLOCK为高时，只能通过串口配置。
    当UNLOCK为低时，自动从EEPROM中加载。

----
## **！注意**:umbrella:
    config  OSC 配置为 0
    MAX1452只能输出正向电压
    目前已知 当UNLOCK为低时 OUT 与 （输入,IRO,PGA,ODAC, OTCDAC）(都有Sign选择)
    UNLOCK从高电平接到低电平或者低电平接到高电平。需要断电，才能生效。

## 调试:christmas_tree:
    1. 可以先看通信有没有成功，IRSP最后几位用于检验  (0xCA)
    2. 看CONFIG 11位CLK1M_EN 用有没有1MHz的信号
    3. 看OUT口的输出，可以先将VDD或VSS输出，看看对不对。


## 之前遇到的问题:当UNLOCK为低时，没有输出。:palm_tree:
    因为ODAC sign和OTCDAC sign 都是0，且ODAC和OTCDAC都是0xFF，所以OUT都减完了。且OSC要设置成000，可是刚开始读到的数据是101，不知道为什么。

## TODO:hourglass:
    校准寄存器配置
    

## 配置流程:sun_with_face:
    **将UNLOCK接到5V**
    1.波特率初始化
    2.页全部擦掉
    3.配置CONFIG，ODAC,OTCDAC
    **将UNLOCK接到0V(！需要断电才能生效)**
    
## 寄存器操作流程:herb:
    通过Serial Interface Command Format，将数据先写入接口寄存器；然后通过Special Command Sequences，将数据写入EEPROM或校准寄存器。















