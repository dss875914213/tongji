#include "max1452.h"
#include "delay.h"
#include "usart.h"

// PGA 增益设置表
int PGA[16]= {39,52,65,78,91,104,117,130,143,156,169,182,195,208,221,234};

// 配置数据
const char CONFIGURATION_HIGH_Byte = 0x0A; //0b00001010;
const char CONFIGURATION_LOW_Byte  = 0x7C; //0b01111100;
const char OTCDAC_HIGH_Byte        = 0x00;
const char OTCDAC_LOW_Byte         = 0x00;
const char ODAC[0x160]			  		 = {0};


void Configure_max1452(void)
{
	Init_baud();
	Erase_EEPROM(Yes,0);
	Configure_Configuration_register(CONFIGURATION_HIGH_Byte, CONFIGURATION_LOW_Byte);
	Configure_OTCDAC_register(OTCDAC_HIGH_Byte, OTCDAC_LOW_Byte);
	Configure_ODAC();
}

void Test_communication(void)
{
	Init_baud();
	Serial_digital_output(IRSP_test);
}



/*---------------------------------------                    波特率操作                                    ------------------------------------------------------------*/
	/*
		功能：
			初始化波特率
	*/
	void Init_baud(void)
	{
		Send_data(0x01);
	}

	/*
		功能：
			重新初始化波特率
	*/
	void Reinit_baud(void)
	{
		Send_data(0xff);
	}

/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    

/*---------------------------------------                  读写！！接口寄存器                              ------------------------------------------------------------v*/

	/*
		输入：
			data:写入寄存器的数据
			Address:写入寄存器的地址（！参考：表9）
		功能：
			将数据写入指定接口寄存器
	*/
	void Serial_interface_command(DATA data,MAX1452_IRSA Address)
	{
		int IRS;
		assert_param(data>=0x0&&data<=0xf);
		IRS = (data<<4)+Address;
		Send_data(IRS);
	}

	/*
		输入：
			interface_register：指定输出数据的寄存器（！参考：表12）
		功能：
			输出指定接口寄存器存储的数据
			串行数字输出，通过串口读取
	*/
	void Serial_digital_output(MAX1452_IRSP interface_register)
	{
		//将数据发送到IRSA_IRSP_3_0
		Serial_interface_command(interface_register,IRSA_IRSP_3_0);

		//将RdIRS写入CRIL接口寄存器
		Serial_interface_command(CRIL_RdIRS,IRSA_CRIL_3_0);
	}

/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------*/


/*---------------------------------------             读写！！校验寄存器   和 配置将内部引脚输出              ------------------------------------------------------------*/
	/*
		输入：
			data：写入寄存器的值
			ICRA：写入哪个寄存器
		功能：
			通过串口将数据发送到寄存器
	*/
	void Write_Register(DATA data, MAX1452_ICRA ICRA)
	{
		assert_param(data>=0x0000&&data<=0xffff);
		//将data写入DHR[15:0]
		Serial_interface_command((data & 0x000f)>> 0, IRSA_DHR_3_0);
		Serial_interface_command((data & 0x00f0)>> 4, IRSA_DHR_7_4);
		Serial_interface_command((data & 0x0f00)>> 8, IRSA_DHR_11_8);
		Serial_interface_command((data & 0xf000)>>12, IRSA_DHR_15_12);

		//将目标寄存器写入ICRA[3:0]
		Serial_interface_command(ICRA, IRSA_ICRA_3_0);
		
		//将加载内部校准寄存器写入CRIL[3:0]
		Serial_interface_command(CRIL_LdICR,IRSA_CRIL_3_0);
	}


	/*
		输入：
			OUT_Pin ：与OUT引脚相连的IO口（！参考：表14）
			ATIM_3_0：模拟信号持续时间（！参考：表13）
		功能：
			将内部指定脚与OUT引脚相连
			多路复用模拟输出
	*/
	void Multiplexed_analog_output(MAX1452_ALOC OUT_Pin,int ATIM_3_0)
	{
		assert_param(ATIM_3_0>=0x0&&ATIM_3_0<=0xf);

		//将数据放入ALOC_3_0
		Serial_interface_command(OUT_Pin,IRSA_ALOC_3_0);

		//将数据放入ATIM_3_0
		Serial_interface_command(ATIM_3_0,IRSA_ATIM_3_0);

		//将RdAlg写入CRIL_3_0
		Serial_interface_command(CRIL_RdAlg,IRSA_CRIL_3_0);
	}

/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------对EEPROM操作，读、写和擦除（只能写0，擦除才能获得1，擦除只能一页一页擦）------------------------------------------------------------*/

	/*
		输入：
			address：EEPROM地址（！参考：表1）
		功能：
			读EEPROM
	*/
	void Read_EEPROM(ADDRESS address)
	{
		assert_param(address>=0x000&&address<=0x2FF);
		// 将地址写入IEEA_9_0
		Serial_interface_command((address&0x0f00)>>8,IRSA_IEEA_9_8);
		Serial_interface_command((address&0x00f0)>>4,IRSA_IEEA_7_4);
		Serial_interface_command((address&0x000f)>>0,IRSA_IEEA_3_0);
		
		// 将IEEA_9_0地址的数据写入DHR_7_0
		Serial_interface_command(CRIL_RdEEP,IRSA_CRIL_3_0);
		
		// 将数据输出
		Serial_digital_output(IRSP_DHR7_0);
	}

	/*
		输入：
			data：写入EEPROM的值
			address：写入的EEPROM的地址（！参考：表1）
		功能：
			写EEPROM
	*/
	void Write_EEPROM(DATA data,ADDRESS address)
	{
		assert_param(address>=0x000&&address<=0x2FF);
		//将数据写入  DHR_7_0
		Serial_interface_command((data&0x0f),IRSA_DHR_3_0);
		Serial_interface_command((data&0xf0)>>4,IRSA_DHR_7_4);
		
		//将地址写入 IEEA_9_0
		Serial_interface_command((address&0x0f00)>>8,IRSA_IEEA_9_8);
		Serial_interface_command((address&0x00f0)>>4,IRSA_IEEA_7_4);
		Serial_interface_command((address&0x000f)>>0,IRSA_IEEA_3_0);
		
		//将EEPW写入CRIL接口寄存器
		Serial_interface_command(CRIL_EEPW,IRSA_CRIL_3_0);
	}

	/*
		输入：
			all_page:1代表擦除所有；0代表不是擦除所有
			page_num:当all_page=0时，指定擦除页（！参考：表1）
		功能：
			擦除EEPROM
	*/
	void Erase_EEPROM(ERASE_all_page all_page,char page_num)
	{
		assert_param(page_num<=0x00&&page_num>=0x0B);
		if(all_page==Yes)//擦除所有EEPROM
		{
			Serial_interface_command(CRIL_ERASE,IRSA_CRIL_3_0);
		}
		else//擦除指定EEPROM
		{
			//将要擦除的EEPROM写入IEEA_9_6
			Serial_interface_command((page_num&0x03)<<2,IRSA_IEEA_7_4);
			Serial_interface_command((page_num&0x0C)>>2,IRSA_IEEA_9_8);

			// 将PageErase写入CRIL寄存器
			Serial_interface_command(CRIL_PageErase,IRSA_CRIL_3_0);
		}	
		//等待至少6ms
		delay_ms(10);
	}
	

	/*
		功能：
			配置EEPROM
			将CL设置成oxff 并配置configuration
	*/
	void Secure_Lock(void)
	{
		//擦除第5页（CL所在页）
		Erase_EEPROM(No,0x05);
	}

	void Configure_Configuration_register(int High_Byte, int Low_Byte)
	{
		//配置configuration
		Write_EEPROM(High_Byte,0x161);
		Write_EEPROM(Low_Byte,0x160);
	}

	void Configure_OTCDAC_register(int High_Byte, int Low_Byte)
	{
		//配置OTCDAC
		Write_EEPROM(High_Byte,0x165);
		Write_EEPROM(Low_Byte,0x164);
	}

	void Configure_FSOTCDAC_register(int High_Byte, int Low_Byte)
	{
		//FSOTCDAC
		Write_EEPROM(High_Byte,0x169);
		Write_EEPROM(Low_Byte,0x168);
	}

	void Configure_ODAC()
	{
		set_ODAC_all_zero();
	}


/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------*/


/*---------------------------------------                       辅助函数                                    ------------------------------------------------------------*/
	/*
		输入：
			data：需要发送的数据
		功能：
			发送数据
	*/
	static void Send_data(int data)
	{
		delay_ms(3);
		USART2->DR = data;
		while((USART2->SR&0X40)==0){;}//等待发送结束
	}

/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------                       临时函数                                     ------------------------------------------------------------*/
	void set_ODAC_all_zero(void)
	{
		int i;
		for(i=0x000;i<=0x15F;i++)
		{
			Write_EEPROM(0x00,i);
		}

	}



	void set_FSODAC_all_zero(void)
	{
		int i;
		for(i=0x16C;i<=0x2FF;i++)
		{
			Write_EEPROM(0x00,i);
		}
	}
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------*/















