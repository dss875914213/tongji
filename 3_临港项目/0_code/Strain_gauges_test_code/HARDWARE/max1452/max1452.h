#ifndef __MAX1452_H
#define __MAX1452_H

#include "sys.h" 


typedef int DATA;
typedef int ADDRESS;



// IRSA�����
typedef enum {
    IRSA_DHR_3_0 = 0X00,
    IRSA_DHR_7_4,
    IRSA_DHR_11_8,
    IRSA_DHR_15_12,
    IRSA_ICRA_3_0 = 0X06,
    IRSA_IEEA_3_0 = 0X06,
    IRSA_IEEA_7_4,
    IRSA_IRSP_3_0,
    IRSA_IEEA_9_8 = 0x08,
    IRSA_CRIL_3_0,
    IRSA_ATIM_3_0,
    IRSA_ALOC_3_0
}MAX1452_IRSA;



// CL ����λ�ÿ���
typedef enum{
    Lock=0x00,
    Unlock=0xff
}MAX1452_CL;



// IRSP ����
typedef enum{
    IRSP_DHR7_0, 			
    IRSP_DHR_15_8, 			
    IRSP_IEEA_7_4_ICRA_3_0, 	
    IRSP_CRIL_3_0_IRSP_3_0, 	
    IRSP_ALOC_3_0_ATIM_3_0, 	
    IRSP_IEEA_7_0, 			
    IRSP_IEED_7_0, 			
    IRSP_TEMP_Index, 				
    IRSP_BitClock,		
    IRSP_test=0x0f	
}MAX1452_IRSP;


// CRIL ������� ��������洢��
typedef enum{
    CRIL_LdICR, 	
    CRIL_EEPW ,    
    CRIL_ERASE,    
    CRIL_RdICR,    
    CRIL_RdEEP,    
    CRIL_RdIRS,    
    CRIL_RdAlg,    
    CRIL_PageErase
}MAX1452_CRIL;


// ICRA����

typedef enum{
    ICRA_CONFIG   ,  
    ICRA_ODAC     ,  
    ICRA_OTCDAC   ,  
    ICRA_FSODAC   ,  
    ICRA_FSOTCDAC
}MAX1452_ICRA;


// ALOC����
typedef enum{
    ALOC_OUT  		,
    ALOC_BDR  		,
    ALOC_ISRC  	    ,
    ALOC_VDD  		,
    ALOC_VSS  		,
    ALOC_BIAS5U  	,
    ALOC_AGND  	    ,
    ALOC_FSODAC  	,
    ALOC_FSOTCDAC 	,
    ALOC_ODAC  	    ,
    ALOC_OTCDAC  	,
    ALOC_VREF  	    ,
    ALOC_VPTATP	    ,
    ALOC_VPTATM  	,
    ALOC_INP  		,
    ALOC_INM  		
}MAX1452_ALOC;


// ��ַ
typedef enum{
	Address_CONFIG_0 = 0x160,
	Address_CONFIG_1,
	Address_CL_0 	 = 0x16A,
	Address_CL_1 	
}MAX1452_Address;


typedef enum{
    No,
    Yes
}ERASE_all_page;





void Send_data(int data);
void Init_baud(void);
void Reinit_baud(void);
void Serial_interface_command(int IRSD,MAX1452_IRSA Address);
void Write_Register(DATA DHR_15_0, MAX1452_ICRA ICRA);
void Erase_EEPROM(ERASE_all_page all_page,char IEEA_9_6);
void Write_EEPROM(int DHR_7_0,int IEEA_9_0);
void Serial_digital_output(MAX1452_IRSP interface_register);
void Read_EEPROM(int IEEA_9_0);
void Secure_Lock(void);
void Multiplexed_analog_output(MAX1452_ALOC OUT_Pin,int ATIM_3_0);
void set_ODAC_all_zero(void);
void set_FSODAC_all_zero(void);
void Configure_Configuration_register(int High_Byte, int Low_Byte);
void Configure_OTCDAC_register(int High_Byte, int Low_Byte);
void Configure_FSOTCDAC_register(int High_Byte, int Low_Byte);
void Configure_ODAC(void);
void Configure_max1452(void);
#endif


