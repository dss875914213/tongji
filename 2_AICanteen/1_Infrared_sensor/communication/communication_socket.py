# -*- coding: utf-8 -*-
import socket
import json
import logging
from parameters import socket_send_ip,socket_receive_ip,socket_port


class SocketInfo:
    def __init__(self,socket_send_ip,socket_receive_ip,socket_port):
        self.__socket_send_ip = socket_send_ip
        self.__socket_receive_ip = socket_receive_ip
        self.__socket_port = socket_port
        try:
            self.__Sockin = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # new socket
            self.__Sockin.bind((self.__socket_send_ip, self.__socket_port))  # socket bind this computer ip and port
        except Exception as e:
            logging.warning(u'建立socket失败，该socket用于调试，建立失败不影响正常使用')
            logging.warning(e)

    def send_data(self, data):
        try:
            data_to_json = json.dumps(data)
            self.__Sockin.sendto(data_to_json, (self.__socket_receive_ip, self.__socket_port))  # sent text to other computer
            # print(data[0])
        except Exception as e:
            logging.warning(u'建立socket失败，该socket用于调试，建立失败不影响正常使用')
            logging.warning(e)




# send_info


