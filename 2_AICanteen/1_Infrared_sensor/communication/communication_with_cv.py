import requests
import json
from parameters import filter_desk
import time


def get_cv_data():
    response = requests.get('http://192.168.31.33:3001/api/occupancy_cv')
    all_data = json.loads(response.text)
    is_out_of_data = False
    per_table_headcount = {}
    for key in filter_desk:
        row_data = all_data[key]
        if row_data['targetType'] == 'table':
            if int(time.time()*1000) - row_data['timestamp'] > 10000:
                is_out_of_data = True
                break
            if 'headcount' in row_data.keys():
                per_table_headcount[row_data['tableID']] = row_data['headcount']
    # print('per_table_headcount', per_table_headcount)

    return is_out_of_data, per_table_headcount

