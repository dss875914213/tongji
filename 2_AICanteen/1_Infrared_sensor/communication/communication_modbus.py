# -*- coding: utf-8 -*-
import serial

class IrSensorModbus:
    def __init__(self):
        self.__ser = serial.Serial(port='/dev/ttyUSB0', baudrate=38400, parity=serial.PARITY_ODD, timeout=0.5)
        self.__read_length = 19


    def __add_crc(self, send_data):
        data = send_data[:]
        crc = self.__cal_crc(data)
        data.append(crc & 0xff)
        data.append(crc >> 8)
        return data

    def __check_crc(self, get_data):
        data = get_data
        crc = self.__cal_crc(data[:-2])
        if data[-1] == crc >> 8 and data[-2] == crc & 0xff:
            return True
        return False

    @staticmethod
    def __cal_crc(data):
        crc = 0xFFFF
        for pos in data:
            crc ^= pos
            for i in range(8):
                if (crc & 1) != 0:
                    crc >>= 1
                    crc ^= 0xA001
                else:
                    crc >>= 1
        return crc

    def __get_raw_data(self,ir_sensor_id):
        send_data = [ir_sensor_id, 0x03, 0x00, 0x00, 0x00, 0x07]
        send_data_add_crc = self.__add_crc(send_data)

        while 1:
            n = self.__ser.write(send_data_add_crc)
            if n != len(send_data_add_crc):
                print("====================Write Fail==============")
                continue
            get_str = self.__ser.read(self.__read_length)
            if len(get_str) == self.__read_length:  # 长度一样
                data = []
                for i in get_str:
                    data.append(ord(i))
                #  data中的是获得的数据，
                #  0是地址，1是function code,2是Byte Count
                # 3和4是人数，5，6，7，8是人的坐标，9，10是存在的数据，11，12，13，14是人的移动方向
                # 15，16是温度信息
                if self.__check_crc(data): # crc校验
                    self.ir_sensor_detected_people_number = data[4]
                    self.__ir_sensor_raw_unit_people_number = [data[5],data[6],data[7],data[8]]
                    break
            else:
                print("==================FAIL=====================", len(get_str))


    def __parse_raw_data(self):
        self.ir_sensor_unit_people_number = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        for person in range(self.ir_sensor_detected_people_number):
            if person % 2 == 0:
                x = (self.__ir_sensor_raw_unit_people_number[person // 2] & 0x03)
                y = (self.__ir_sensor_raw_unit_people_number[person // 2] & 0x0c) >> 2
            else:
                x = (self.__ir_sensor_raw_unit_people_number[person // 2] & 0x30) >> 4
                y = (self.__ir_sensor_raw_unit_people_number[person // 2] & 0xc0) >> 6
            self.ir_sensor_unit_people_number[x + y * 4] += 1


    def get_parsed_data(self,ir_sensor_id):
        self.__get_raw_data(ir_sensor_id)
        self.__parse_raw_data()
        # self.ir_sensor_detected_people_number = 2
        # self.ir_sensor_unit_people_number = [0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]

ir_sensor_modbus = IrSensorModbus()




