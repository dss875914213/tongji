# -*- coding: utf-8 -*-
import pika
import json
import time
import logging

from parameters import rabbitmq_name, rabbitmq_pwd, rabbitmq_host_ip, rabbitmq_host_port
from parameters import rabbitmq_queue_number, rabbitmq_queue_name


class _RabbitmqQueue:
    def __init__(self, my_queue_name, rabbitmq_name, rabbitmq_pwd, rabbitmq_host_ip, rabbitmq_host_port):
        self.__mq_name = rabbitmq_name
        self.__mq_pwd = rabbitmq_pwd
        self.__mq_host = rabbitmq_host_ip
        self.__mq_port = rabbitmq_host_port
        self.__mq_queue = my_queue_name
        self.__mq_routing_key = my_queue_name
        self.__connect_to_server()

    def __connect_to_server(self):
        try:
            credentials = pika.PlainCredentials(self.__mq_name, self.__mq_pwd)
            s_conn = pika.BlockingConnection(pika.ConnectionParameters(
                host=self.__mq_host, port=self.__mq_port, credentials=credentials))
            self.__chan = s_conn.channel()
            self.__chan.queue_declare(queue=self.__mq_queue)
        except Exception as e:
            logging.error("！！退出程序。初始化时与rabbitmq建立连接失败，可能192.168.31.33服务器处有问题。")
            logging.error(e)
            exit()

    def send_data_inner(self, data):
        data['timestamp'] = int(time.time() * 1000)
        mq_body = json.dumps(data)
        self.__chan.basic_publish(exchange='', routing_key=self.__mq_routing_key, body=mq_body)


class AllRabbitmqQueue:
    def __init__(self, queue_number, queue_name):
        self.__queue_number = queue_number
        self.__queue_name = queue_name
        self.__all_rabbitmq_queue = {}

        # function
        self.__create_rabbitmq_queue()

    def __create_rabbitmq_queue(self):
        for queue_id in range(self.__queue_number):
            self.__all_rabbitmq_queue[queue_id] = \
                _RabbitmqQueue(self.__queue_name[queue_id], rabbitmq_name, rabbitmq_pwd, rabbitmq_host_ip,
                               rabbitmq_host_port)

    def send_data(self, queue_id, message):
        try:
            self.__all_rabbitmq_queue[queue_id].send_data_inner(message)
        except Exception as e:
            logging.error("发送数据到rabbitmq失败")
            logging.error('e')
