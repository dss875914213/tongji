import socket
import sys
import json
sys.path.append('../')
from parameters import socket_receive_ip,socket_port

class SocketInfo:
    def __init__(self):
        self.__sockin = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.__sockin.bind((socket_receive_ip,socket_port))
        self.__msq = None
        self.table_people_number = None
        self.people_position = None

    def __receive_data(self):
        self.__msq,(addr,port) = self.__sockin.recvfrom(100000)
        self.__msq = json.loads(self.__msq)

    def __parse_data(self):
        self.people_position = self.__msq[0]
        self.table_people_number = self.__msq[1]
        print(self.people_position)
    def get_data(self):
        self.__receive_data()
        self.__parse_data()

if __name__ == '__main__':
    socketinfo = SocketInfo()
    socketinfo.get_data()
    print(socketinfo.people_position)
    print(socketinfo.table_people_number)








