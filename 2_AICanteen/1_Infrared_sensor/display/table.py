import json
import matplotlib.pyplot as plt
import sys
sys.path.append('../data')

Table_Shape = ['square','two_square','rectangle','circle','big_circle','rotate_two_square']
Side_Length_Or_Diameter = {
    Table_Shape[0]:[0.91,0.91],
    Table_Shape[1]:[0.91,0.91*2],
    Table_Shape[2]:[2.5, 1.1],
    Table_Shape[3]:1,
    Table_Shape[4]:1.5}

All_Table_Shape =[Table_Shape[5], Table_Shape[5], Table_Shape[5],Table_Shape[5], Table_Shape[5], Table_Shape[5],
             Table_Shape[3], Table_Shape[0], Table_Shape[0],Table_Shape[3], Table_Shape[3], Table_Shape[3],
             Table_Shape[3], Table_Shape[2], Table_Shape[2],Table_Shape[3], Table_Shape[3], Table_Shape[3],
             Table_Shape[4], Table_Shape[0], Table_Shape[1],Table_Shape[0], Table_Shape[0], Table_Shape[1]]

class _Table:
    def __init__(self,table_id, table_shape,table_center=None,side_length_or_diameter=None,vertex_of_table=None):
        self.__table_id = table_id
        self.__table_shape = table_shape
        self.__table_center = table_center
        self.__side_length_or_diameter = side_length_or_diameter
        self.__vertex_of_table = vertex_of_table
        self.__table_color = 'deepskyblue'

    def draw_table(self, desk_people_number):
        text_x,text_y = 0,0
        if self.__table_shape in [Table_Shape[0],Table_Shape[1],Table_Shape[2]]: #矩形类
            x_start = self.__table_center[0] - self.__side_length_or_diameter[0] / 2
            y_start = self.__table_center[1] - self.__side_length_or_diameter[1] / 2
            x_size = self.__side_length_or_diameter[0]
            y_size = self.__side_length_or_diameter[1]
            rect = plt.Rectangle((x_start, y_start), x_size, y_size, edgecolor=self.__table_color, facecolor='none')
            plt.gca().add_patch(rect)
            text_x = self.__table_center[0] + 0.5
            text_y = self.__table_center[1]
        elif self.__table_shape in [Table_Shape[3],Table_Shape[4]]: #圆形
            x_start = self.__table_center[0]
            y_start = self.__table_center[1]
            cir1 = plt.Circle(xy=(x_start, y_start), radius=self.__side_length_or_diameter/2,
                              edgecolor=self.__table_color, facecolor='none')
            plt.gca().add_patch(cir1)
            text_x = self.__table_center[0] + 0.5
            text_y = self.__table_center[1]
        elif self.__table_shape in [Table_Shape[5]]: #2边
            plt.plot([self.__vertex_of_table[0][0], self.__vertex_of_table[1][0]],
                     [self.__vertex_of_table[0][1], self.__vertex_of_table[1][1]], c=self.__table_color)
            plt.plot([self.__vertex_of_table[0][0], self.__vertex_of_table[2][0]],
                     [self.__vertex_of_table[0][1], self.__vertex_of_table[2][1]], c=self.__table_color)
            text_x = self.__vertex_of_table[0][0] + 0.5
            text_y = self.__vertex_of_table[0][1]

        plt.text(text_x, text_y, self.__table_id+1, c='deepskyblue', fontsize=8)
        plt.text(text_x, text_y + 0.5, desk_people_number, c='r', fontsize=8)


class AllTable:
    def __init__(self):
        self.table_center_dir = '../data/filter_data.json'
        self.table_vertex_dir = '../data/filter_data_3point.json'
        self.table_center = json.load(open(self.table_center_dir))
        self.table_vertex = json.load(open(self.table_vertex_dir))
        self.__all_table_shape = All_Table_Shape
        self.__table_number = 24
        self.__all_table = {}
        # function
        self.__create_table()

    def __create_table(self):
        for table_id in range(self.__table_number):
            if All_Table_Shape[table_id] == Table_Shape[-1]:
                self.__all_table[table_id] = _Table(table_id, All_Table_Shape[table_id],\
                                                    vertex_of_table=[self.table_vertex[0][str(table_id+1)],
                                                                     self.table_vertex[1][str(table_id+1)],
                                                                     self.table_vertex[2][str(table_id+1)]])
            else:
                self.__all_table[table_id] = _Table(table_id, All_Table_Shape[table_id],
                                                    table_center=self.table_center[0][str(table_id+1)],
                                                    side_length_or_diameter=Side_Length_Or_Diameter[All_Table_Shape[table_id]])

    def draw_all_table(self,desk_people_number):
        for table_id in self.__all_table:
            self.__all_table[table_id].draw_table(desk_people_number[str(table_id+1)])




if __name__ == '__main__':
    plt.figure(1)
    plt.xlim(-11, 11)
    plt.ylim(-22, 10)
    all_desk = AllTable()
    all_desk.draw_all_table({'1': 1, '2': 0, '3': 0, '4': 0, '5': 0, '6': 0, '7': 0, '8': 0, '9': 0, '10': 0, '11': 0, '12': 0, '13': 0, '14': 0, '15': 0, '16': 0, '17': 0,
 '18': 0, '19': 0, '20': 0, '21': 0, '22': 0, '23': 1, '24': 0, 'timestamp': 1576385203577})
    plt.ion()
    plt.pause(20)
    plt.clf()