# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
from parameters import all_ir_sensor_unit_position,exist_ir_sensor


class _IrSensor:
    def __init__(self,ir_sensor_id, ir_sensor_unit_position):
        self.__ir_sensor_id = ir_sensor_id
        self.__ir_sensor_unit_position = ir_sensor_unit_position

    def draw_ir_sensor_unit(self, colors):
        for unit_id in range(16):
            x, y = self.__ir_sensor_unit_position[unit_id]
            plt.scatter(x, y, marker='o', color=colors, label='1', s=2)

class AllIrSensor:
    def __init__(self):
        self.__exist_ir_sensor = exist_ir_sensor
        self.__all_ir_sensor_unit_position = all_ir_sensor_unit_position
        self.__all_ir_sensor = {}

        # funcition
        self.__create_ir_sensor()

    def __create_ir_sensor(self):
        for ir_sensor_id in self.__exist_ir_sensor:
            self.__all_ir_sensor[ir_sensor_id] = _IrSensor(ir_sensor_id,
                                                           self.__all_ir_sensor_unit_position[ir_sensor_id])

    def draw_all_ir_sensor_unit(self):
        colors = ['b','g','y','m']
        for ir_sensor_id in self.__exist_ir_sensor:
            self.__all_ir_sensor[ir_sensor_id].draw_ir_sensor_unit(colors[ir_sensor_id%4])

if __name__ == "__main__":
    plt.figure(1)
    plt.xlim(-11, 11)
    plt.ylim(-22, 10)
    all_ir_sensor = AllIrSensor()
    all_ir_sensor.draw_all_ir_sensor_unit()
    plt.ion()
    plt.pause(20)
    plt.clf()





