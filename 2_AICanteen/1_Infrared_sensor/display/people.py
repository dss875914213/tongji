# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt

class People:
    def draw_people_position(self, all_people_position):
        for people in all_people_position:
            x, y = all_people_position[people]
            plt.scatter(x, y, marker='*', color='y', label='1', s=80)


if __name__ == '__main__':
    all_people_position = {'0': [4.1162304964539, -14.026244680851063], '1': [4.314166666666666, -16.77765476190476],
     '2': [3.3764999999999996, -13.96465476190476], '3':[4.4015, -16.5985], '4': [-3.9562236842105265, -13.82796052631579],
     '5': [1.3535890410958904, -11.623150684931506], '6': [0.15145898803902935, -1.07653636425239],
     '7': [-0.24265679548531227, -1.9222087294015617]}
    plt.figure(1)
    plt.xlim(-11, 11)
    plt.ylim(-22, 10)
    people = People()
    people.draw_people_position(all_people_position)
    plt.ion()
    plt.pause(20)
    plt.clf()







