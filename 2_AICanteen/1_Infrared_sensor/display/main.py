# -*- coding: utf-8 -*-

# 1.接收并解析数据
# 2.画桌子位置和桌子上的人数
# 3.红外阵列传感器单元位置
# 4.显示人的位置

import socket
import time
from table import AllTable
from communication_by_socket_receive import SocketInfo
from ir_sensor import AllIrSensor
from people import People
import matplotlib.pyplot as plt
import matplotlib
# matplotlib.use('Agg')

class main_function:

    def init(self):
        plt.figure(1)
        plt.xlim(-11, 11)
        plt.ylim(-22, 0)
        self.socketinfo = SocketInfo()
        self.all_desk = AllTable()
        self.all_ir_sensor = AllIrSensor()
        self.people = People()

    def run_once(self):
        pass

    def keep_running(self):
        global image
        self.socketinfo.get_data()
        self.all_desk.draw_all_table(self.socketinfo.table_people_number)
        self.all_ir_sensor.draw_all_ir_sensor_unit()
        self.people.draw_people_position(self.socketinfo.people_position)
        # plt.show()
        plt.ion()
        plt.pause(0.1)
        plt.clf()

if __name__ == '__main__':
    IR_SENSOR = main_function()
    IR_SENSOR.init()
    while 1:
        IR_SENSOR.keep_running()
        # time.sleep(0.01)


