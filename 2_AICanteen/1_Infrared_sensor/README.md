## time 			: 2020/1/7
## author 			: dss
## description 		: 通过树莓派采集红外阵列传感器，获取餐厅人人员分布热力图。(通过 modbus 协议采集数据)
					  将数据发送到服务端，供屏幕上显示。(发送到 rabbitmq ，然后陆老师到 rabbitmq 上获取数据，并展示)
					  当跳舞机器人上的红外阵列传感器感应到有人时，发送命令控制机器人运动。(控制机器人相关文档，在 doc/robot)
## run 				: 目前是自启动，关闭自启动后，通过 python main.py 运行。
					  通过 sudo /usr/bin/supervisorctl stop all 指令关闭自启动。
					  通过 sudo /usr/bin/supervisord -c /etc/supervisor/supervisord.conf 开启自启动。
## doc				: notion 上有红外阵列传感器的相关文档。  		

树莓派 
ip 地址：192.168.20.241
账号: pi 
密码：123

rabbitmq （树莓派发送到5672，但是这个端口网上不能直接查看。15672这个端口网上可以直接查看）
ip 地址：192.168.31.33
端口：5672
账号：helab
密码：helab

跳舞机器人
ip 地址：192.168.20.234
端口：5678
账号：tjai
密码：tjai



### other
http://192.168.31.33:3001/   实时显示人数
http://192.168.31.33:15672/  rabbitmq
2b81u26256.wicp.vip:17858/   mysql
客如云测试
标定机器人边上的红外阵列








