# -*- coding: utf-8 -*-
__author__ = 'dss'

# 系统自带的
import argparse
import logging
import time
import sys

# 自己写的
sys.path.append('../')
sys.path.append('Object')
sys.path.append('communication')
from parameters import *
from ir_sensor import AllIrSensor
from communication_rabbitmq import AllRabbitmqQueue
from table import AllTable
from communication_socket import SocketInfo
from robot import Robot
from communication_with_cv import get_cv_data
import collections
import numpy as np


class main_function:
    def init(self):
        reload(sys)
        sys.setdefaultencoding('gbk')  # @UndefinedVariable

        logging.basicConfig(level=logging.INFO, format='%(asctime)s-%(levelname)s:%(message)s')
        # logging.basicConfig(filename="log/my_ir_sensor.log", level=logging.WARNING,
        #                     format='%(asctime)s-%(levelname)s:%(message)s')
        self.ir_sensors = AllIrSensor(ir_sensor_number, exist_ir_sensor, all_ir_sensor_unit_position)
        self.all_table = AllTable(all_table_ir_sensor_relate, desk_number, exist_desk)
        self.all_rabbitmq_queue = AllRabbitmqQueue(rabbitmq_queue_number, rabbitmq_queue_name)
        # self.socket_info = SocketInfo(socket_send_ip, socket_receive_ip, socket_port)
        self.dance_robot = Robot()
        self.dance_robot_dancing_flag = 0

        parser = argparse.ArgumentParser(description='manual to this script')
        parser.add_argument('--debug', type=int, default=1)
        self.args = parser.parse_args()

        self.filter_per_table_people_number = {}
        self.save_ir_per_table_people_number = [collections.deque() for _ in range(13)]
        self.save_cv_per_table_people_number = [collections.deque() for _ in range(13)]
        self.save_is_per_table_people_number_increase = [False for _ in range(13)]

    def run_once(self):
        pass

    def keep_running(self):
        # 1. get people_position
        self.ir_sensors.get_all_data()

        # 2. get desk_people_number
        self.all_table.cal_all_desk_people_number(self.ir_sensors.all_ir_sensor_unit_people_number)

        # 4. send data by socket
        # self.socket_info.send_data([self.ir_sensors.all_people_position, self.all_table.per_table_people_number])

        # 5. get cv data
        try:
            is_out_of_data, per_table_headcount = get_cv_data()
            have_cv_data = ~is_out_of_data
            # print('per_table_headcount1', per_table_headcount)
        except:
            have_cv_data = False
            print('不能获取摄像头每个桌子人数的数据')

        self.filter_per_table_people_number = self.all_table.per_table_people_number
        # 6. data filter
        if have_cv_data:
            for desk_ID in filter_desk:
                temp_index = int(desk_ID) - 1
                if len(self.save_cv_per_table_people_number[temp_index]) < 10:
                    self.save_cv_per_table_people_number[temp_index].append(per_table_headcount[int(desk_ID)])
                else:
                    self.save_cv_per_table_people_number[temp_index].append(per_table_headcount[int(desk_ID)])
                    self.save_cv_per_table_people_number[temp_index].popleft()

                if len(self.save_ir_per_table_people_number[temp_index]) < 10:
                    self.save_ir_per_table_people_number[temp_index]. \
                        append(self.all_table.per_table_people_number[int(desk_ID)])
                else:
                    self.save_ir_per_table_people_number[temp_index]. \
                        append(self.all_table.per_table_people_number[int(desk_ID)])
                    self.save_ir_per_table_people_number[temp_index].popleft()

                # increase believe ir more, decrease believe cv more
                if len(self.save_ir_per_table_people_number[temp_index]) < 5 or self.save_cv_per_table_people_number[temp_index] < 5:
                    cv_proportion = 0.5
                    ir_proportion = 0.5
                else:
                    if self.save_ir_per_table_people_number[temp_index][-1] > self.save_ir_per_table_people_number[temp_index][-2]:
                        self.save_is_per_table_people_number_increase[temp_index] = True
                    if self.save_cv_per_table_people_number[temp_index][-1] < self.save_cv_per_table_people_number[temp_index][-2]:
                        self.save_is_per_table_people_number_increase[temp_index] = False

                    var_cv = np.var(self.save_cv_per_table_people_number[temp_index])
                    var_ir = np.var(self.save_ir_per_table_people_number[temp_index])
                    if self.save_is_per_table_people_number_increase[temp_index]:
                        cv_proportion = (var_ir + 1) / (var_cv + var_ir + 2) - 0.1
                        ir_proportion = (var_cv + 1) / (var_cv + var_ir + 2) + 0.1
                    else:
                        cv_proportion = (var_ir + 1) / (var_cv + var_ir + 2) + 0.1
                        ir_proportion = (var_cv + 1) / (var_cv + var_ir + 2) - 0.1

                self.filter_per_table_people_number[int(desk_ID)] = int(cv_proportion * per_table_headcount[int(desk_ID)] \
                        + ir_proportion * self.all_table.per_table_people_number[int(desk_ID)]+0.5)
            print('ir: ', self.all_table.per_table_people_number)
            print('cv: ', per_table_headcount)
            print('filter: ', self.filter_per_table_people_number)
            print('isIncrease: ', self.save_is_per_table_people_number_increase)
            print('')

        else:
            self.filter_per_table_people_number = {}
            self.save_ir_per_table_people_number = [collections.deque() for _ in range(13)]
            self.save_cv_per_table_people_number = [collections.deque() for _ in range(13)]
            self.save_is_per_table_people_number_increase = [False for _ in range(13)]

        # 3. send data to rabbitmq
        self.all_rabbitmq_queue.send_data(0, self.filter_per_table_people_number)
        self.all_rabbitmq_queue.send_data(1, self.ir_sensors.all_people_position)
        # self.all_rabbitmq_queue.send_data(2, self.filter_per_table_people_number)


        # logging.info(self.all_table.per_table_people_number)
        # logging.info(self.ir_sensors.all_people_position)

        if self.ir_sensors.detect_human_in_front_of_robot_ir_sensor.ir_sensor_detected_people_number:
            if self.args.debug == 1:
                # if self.ir_sensors.detect_human_in_front_of_robot_ir_sensor.all_ir_sensor_unit_people_number[]:
                used_unit = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0]
                for (i, j) in zip(self.ir_sensors.detect_human_in_front_of_robot_ir_sensor.ir_sensor_unit_people_number,
                                  used_unit):
                    if i & j == 1:
                        print(self.ir_sensors.detect_human_in_front_of_robot_ir_sensor.ir_sensor_unit_people_number)
                        self.dance_robot.control_robot()


if __name__ == '__main__':
    IR_SENSOR = main_function()
    IR_SENSOR.init()
    while 1:
        IR_SENSOR.keep_running()
        time.sleep(2)
