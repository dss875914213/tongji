class ModbusRtu:

    def __init__(self):
        pass

    def add_crc(self, input_data):
        data = input_data[:]
        crc = self.crc_cal(data)
        data.append(crc & 0xff)
        data.append(crc >> 8)
        return data

    def check_crc(self, get_data):
        data = get_data
        crc = self.crc_cal(data[:-2])
        if data[-1] == crc >> 8 and data[-2] == crc & 0xff:
            return True
        return False

    @staticmethod
    def crc_cal(data):
        crc = 0xFFFF
        for pos in data:
            crc ^= pos
            for i in range(8):
                if (crc & 1) != 0:
                    crc >>= 1
                    crc ^= 0xA001
                else:
                    crc >>= 1
        return crc



