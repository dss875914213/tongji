import pika
import sys
import json
import time

class send_to_rabbitmq:
    def __init__(self,my_queue_name):
        self.__mq_name = 'helab'
        self.__mq_pwd = 'helab'
        self.__mq_host = '192.168.31.33'
        self.__mq_port = 5672

        self.__mq_queue = my_queue_name
        self.__mq_routing_key =my_queue_name
        self.__connect_to_server()

    def __connect_to_server(self):
        credentials = pika.PlainCredentials(self.__mq_name, self.__mq_pwd)

        s_conn = pika.BlockingConnection(pika.ConnectionParameters(
            host=self.__mq_host, port=self.__mq_port, credentials=credentials))

        self.__chan = s_conn.channel()

        self.__chan.queue_declare(queue=self.__mq_queue)

    def send_data(self, data):
        mq_body = json.dumps(data)
        self.__chan.basic_publish(exchange='', routing_key=self.__mq_routing_key, body=mq_body)
        print("[sheng chang zhe] send '%s'" % mq_body)
        #TODO

# s_conn.close()

