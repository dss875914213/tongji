from math import sin, cos
import matplotlib.pyplot as plt
import json
import sys
sys.path.append('data')
# 传感器中心坐标，角度，检测范围
IR_sensor_info = {1: [], 2: [], 3: [], 4: [], 5: [], 6: [], 7: [], 8: [], 9: [], 10: [],
                  11: [], 12: [], 13: [], 14: [], 15: [], 16: [], 17: [], 18: [], 19: [],
                  20: [], 21: []}

IR_sensor_unit_location = {1: [], 2: [], 3: [], 4: [], 5: [], 6: [], 7: [], 8: [], 9: [], 10: [],
                          11: [], 12: [], 13: [], 14: [], 15: [], 16: [], 17: [], 18: [], 19: [],
                          20: [], 21: []}
# 每个位置有没有人，以及个数  外部获得的数据
IR_sensor_detect = {3: [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1],
                    4: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
                    5: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    6: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    7: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    8: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    9: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    10: [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    11: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
                    12: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    13: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    14: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    15: [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    16: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    17: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    18: [0, 0, 0, 0, 1, 2, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
                    19: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    20: [0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0],
                    21: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]}

class add_IR_sensor_info:
    def __init__(self):
        self.__IR_center_position_dir = 'IR_sensor.json'
        self.__IR_sensor_area_dir = 'IR_sensor_side_length.json'
        self.__different_angle = [12, 13, 17, 18]
        self.add_IR_sensor_info()

    def __add_center_position(self):
        IR_center_position = json.load(open(self.__IR_center_position_dir))
        for i in range(3, 22):
            IR_sensor_info[i].append(IR_center_position[0][str(i)])

    def __add_detection_area_position(self):
        IR_sensor_area = json.load(open(self.__IR_sensor_area_dir))
        for i in range(3, 22):
            IR_sensor_info[i].append(IR_sensor_area[str(i)])

    def __add_angle(self):
        for i in range(3, 22):
            if i in self.__different_angle:
                IR_sensor_info[i].append(-25)
            else:
                IR_sensor_info[i].append(0)

    def add_IR_sensor_info(self):
        self.__add_center_position()
        self.__add_angle()
        self.__add_detection_area_position()
        print(IR_sensor_info)


# 每个传感器每个单元中心坐标
def cal_IR_sensor_unit_location(IR_sensor_id):
    # 考虑IR传感器中心位置和角度
    fig = plt.figure()
    plt.xlim(-11, 11)
    plt.ylim(-22, 10)

    for sensor_id in range(3, 22):
        center_location = IR_sensor_info[sensor_id][0]
        angle = IR_sensor_info[sensor_id][1]
        side_length = IR_sensor_info[sensor_id][2]
        unit_location = []

        distance = [[-0.375, 0.375], [-0.125, 0.375], [0.125, 0.375], [0.375, 0.375],
                    [-0.375, 0.125], [-0.125, 0.125], [0.125, 0.125], [0.375, 0.125],
                    [-0.375, -0.125], [-0.125, -0.125], [0.125, -0.125], [0.375, -0.125],
                    [-0.375, -0.375], [-0.125, -0.375], [0.125, -0.375], [0.375, -0.375]]

        for unit_id in range(16):
            temp_x = center_location[0] + \
                     sin(angle / 180 * 3.14) * (distance[unit_id][0] * side_length) + \
                     cos(angle / 180 * 3.14) * (distance[unit_id][1] * side_length)
            temp_y = center_location[1] - \
                     cos(angle / 180 * 3.14) * (distance[unit_id][0] * side_length) + \
                     sin(angle / 180 * 3.14) * (distance[unit_id][1] * side_length)
            unit_location.append([temp_x, temp_y])
        for x, y in unit_location:
            plt.scatter(x, y, marker='x', color='m', label='1', s=5)
        rect = plt.Rectangle(center_location, side_length / 2, side_length / 2, edgecolor='y', facecolor='none',
                             angle=angle)

        plt.gca().add_patch(rect)
        IR_sensor_unit_location[sensor_id] = unit_location
        print(IR_sensor_unit_location)
    plt.show()
    print(unit_location)




def cal_desk_people_num():
    desk_people_number = {}
    for desk_id in range(1, 25):
        people_num = 0
        for ir_sensor in desk_ir_sensor_relate[desk_id]:
            if len(ir_sensor) == 0:
                continue
            for i in ir_sensor.keys():
                ir_sensor_id = i
            ir_sensor_unit = ir_sensor[ir_sensor_id]
            for j in ir_sensor_unit:
                # print('here', ir_sensor_id,j)
                people_num += IR_sensor_detect[ir_sensor_id][j-1]
        print(desk_id, people_num)
        desk_people_number[desk_id] = people_num
    print(desk_people_number)
    return desk_people_number


def heat_map():
    # TODO
    pass






if __name__ == "__main__":

    # cal_desk_people_num()
    IR_sensor_info1 = add_IR_sensor_info()
    cal_IR_sensor_unit_location(1)
    #
