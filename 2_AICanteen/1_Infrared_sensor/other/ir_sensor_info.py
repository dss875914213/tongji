import json
import matplotlib.pyplot as plt
from math import sin,cos
import time
from threading import Thread
import sys
sys.path.append('data')

ir_sensor_unit_people ={
1: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
 2: [0, 0, 0, 0, 0, 0, 0, 0 , 0, 0, 0, 0, 0, 0, 0, 0],
 3: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1],
 4: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
 5: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
 6: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
 7 : [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
 8: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
 9: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
 10: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
 11: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
 12: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
 13: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
 14: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
 15: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]       ,
 16: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
 17: [0, 0, 0, 0, 0, 0, 0 , 0, 0, 0, 0, 0, 0, 0, 0, 0],
 18: [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,  0],
 19: [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
 20: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
 21: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 , 0]}

class add_IR_sensor_info:
    def __init__(self):
        self.__IR_center_position_dir = 'IR_sensor.json'
        self.__IR_sensor_area_dir = 'IR_sensor_side_length.json'
        self.__different_angle_ir_sensor = [12, 13, 17, 18]
        self.__used_ir_sensor = [3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21]
        self.__unit_position = [[-0.375, 0.375], [-0.125, 0.375], [0.125, 0.375], [0.375, 0.375],
                                [-0.375, 0.125], [-0.125, 0.125], [0.125, 0.125], [0.375, 0.125],
                                [-0.375, -0.125], [-0.125, -0.125], [0.125, -0.125], [0.375, -0.125],
                                [-0.375, -0.375], [-0.125, -0.375], [0.125, -0.375], [0.375, -0.375]]
        self.angle = {}
        self.center_position = {}
        self.detect_area = {}
        self.IR_sensor_unit_position = {}
        self.__cal_center_position()
        self.__cal_detection_area_position()
        self.__cal_install_angle()

    def __cal_center_position(self):
        IR_center_position = json.load(open(self.__IR_center_position_dir))
        for i in self.__used_ir_sensor:
            self.center_position[i] = IR_center_position[0][str(i)]

    def __cal_detection_area_position(self):
        IR_sensor_area = json.load(open(self.__IR_sensor_area_dir))
        for i in self.__used_ir_sensor:
            self.detect_area[i] = IR_sensor_area[str(i)]

    def __cal_install_angle(self):
        for i in self.__used_ir_sensor:
            if i in self.__different_angle_ir_sensor:
                self.angle[i] = -25
            else:
                self.angle[i] = 0

    def cal_IR_sensor_unit_info(self):
        # fig = plt.figure()
        # plt.xlim(-11, 11)
        # plt.ylim(-22, 10)
        for sensor_id in self.__used_ir_sensor:
            unit_location = []
            for unit_id in range(16):
                temp_x = self.center_position[sensor_id][0] + \
                         sin(self.angle[sensor_id] / 180 * 3.14) * (self.__unit_position[unit_id][0] * self.detect_area[sensor_id]) + \
                         cos(self.angle[sensor_id] / 180 * 3.14) * (self.__unit_position[unit_id][1] * self.detect_area[sensor_id])
                # print(self.detect_area[sensor_id], self.angle[sensor_id], self.__unit_position[unit_id][0])
                temp_y = self.center_position[sensor_id][1] - \
                         cos(self.angle[sensor_id] / 180 * 3.14) * (self.__unit_position[unit_id][0] * self.detect_area[sensor_id]) + \
                         sin(self.angle[sensor_id] / 180 * 3.14) * (self.__unit_position[unit_id][1] * self.detect_area[sensor_id])
                unit_location.append([temp_x, temp_y])
            for x, y in unit_location:
                plt.scatter(x, y, marker='x', color='m', label='1', s=5)
            rect = plt.Rectangle(self.center_position[sensor_id], self.detect_area[sensor_id] / 2, self.detect_area[sensor_id] / 2,
                                 edgecolor='y', facecolor='none', angle=self.angle[sensor_id])

            plt.gca().add_patch(rect)
            self.IR_sensor_unit_position[sensor_id] = unit_location
        # plt.show()
        print(self.IR_sensor_unit_position)

    def draw_exit_ir_sensor_unit(self, ir_sensor_unit_people):
        colors = ['b','g','y','m']
        for sensor_id in self.__used_ir_sensor:
            for unit_id in range(16):
                if ir_sensor_unit_people[str(sensor_id)][unit_id] != 0:
                    x,y = self.IR_sensor_unit_position[sensor_id][unit_id]
                    plt.scatter(x, y, marker='x', color='r', label='1', s=10)
                else:
                    x, y = self.IR_sensor_unit_position[sensor_id][unit_id]
                    plt.scatter(x, y, marker='o', color=colors[sensor_id%4], label='1', s=2)
        # plt.close(1)


if  __name__ == '__main__':
    sensor_info = add_IR_sensor_info()
    print('dss')
    sensor_info.cal_IR_sensor_unit_info()
    print('syy')
    while(1):
        sensor_info.draw_exit_ir_sensor_unit(ir_sensor_unit_people)