import json
import matplotlib.pyplot as plt
import sys
sys.path.append('data')

class desk:
    desk_center_dir = 'filter_data.json'
    desk_edge_dir = 'filter_data_3point.json'
    desk_center = json.load(open(desk_center_dir))
    desk_edge = json.load(open(desk_edge_dir))
    desk_type = ['x', 'C', 'C', 'C', 'C', 'C', 'C', 'B', 'A', 'A', 'B', 'B', 'B', 'B', 'E', 'E', 'B', 'B', 'B', 'D',
                 'A', 'C', 'A', 'A', 'C']
    desk_type_size = dict(A=[0.91, 0.91], B=0.5, C=[0.91, 0.91 * 2], D=0.75, E=[2.5, 1.1])


    def __init__(self):
        pass

    def draw_desk_by_center(self, desk_people_number):
        for i in range(7, 25):
            print(i)
            if self.desk_type[i] in ['A', 'E']:
                x_start = self.desk_center[0][str(i)][0] - self.desk_type_size[self.desk_type[i]][0] / 2
                y_start = self.desk_center[0][str(i)][1] - self.desk_type_size[self.desk_type[i]][1] / 2
                x_size = self.desk_type_size[self.desk_type[i]][0]
                y_size = self.desk_type_size[self.desk_type[i]][1]
                rect = plt.Rectangle([x_start, y_start], x_size, y_size, edgecolor='deepskyblue', facecolor='none')
                plt.gca().add_patch(rect)
                text_x = self.desk_center[0][str(i)][0] + 0.5
                text_y = self.desk_center[0][str(i)][1]

            elif self.desk_type[i] in ['B', 'D']:
                x_start = self.desk_center[0][str(i)][0]
                y_start = self.desk_center[0][str(i)][1]
                cir1 = plt.Circle(xy=(x_start, y_start), radius=self.desk_type_size[self.desk_type[i]],
                                  edgecolor='deepskyblue', facecolor='none')
                plt.gca().add_patch(cir1)
                text_x = self.desk_center[0][str(i)][0] + 0.5
                text_y = self.desk_center[0][str(i)][1]
            elif self.desk_type[i] in ['C']:
                x_start = self.desk_center[0][str(i)][0] - self.desk_type_size[self.desk_type[i]][0] / 2
                y_start = self.desk_center[0][str(i)][1] - self.desk_type_size[self.desk_type[i]][1] / 2
                x_size = self.desk_type_size[self.desk_type[i]][0]
                y_size = self.desk_type_size[self.desk_type[i]][1]
                rect = plt.Rectangle([x_start, y_start], x_size, y_size, edgecolor='deepskyblue', facecolor='none')
                plt.gca().add_patch(rect)
                text_x = self.desk_center[0][str(i)][0] + 0.5
                text_y = self.desk_center[0][str(i)][1]

            plt.text(text_x, text_y, i, c='deepskyblue', fontsize=8)
            plt.text(text_x, text_y+0.5, desk_people_number[i],c='r',fontsize=8)

    def draw_desk_by_edge(self, desk_people_number):
        for i in range(1, 7):
            print(self.desk_edge[0][str(i)])

            plt.plot([self.desk_edge[0][str(i)][0], self.desk_edge[1][str(i)][0]],
                     [self.desk_edge[0][str(i)][1], self.desk_edge[1][str(i)][1]],c='deepskyblue')
            plt.plot([self.desk_edge[0][str(i)][0], self.desk_edge[2][str(i)][0]],
                     [self.desk_edge[0][str(i)][1], self.desk_edge[2][str(i)][1]],c='deepskyblue')
            plt.text(self.desk_edge[0][str(i)][0] + 0.5, self.desk_edge[0][str(i)][1],
                     i, c='deepskyblue', fontsize=8)
            plt.text(self.desk_edge[0][str(i)][0] + 0.5, self.desk_edge[0][str(i)][1] + 0.5,
                     desk_people_number[i], c='r', fontsize=8)

if __name__ == '__main__':
    plt.figure(1)
    plt.xlim(-11, 11)
    plt.ylim(-22, 10)
    all_desk = desk()
    all_desk.draw_desk_by_center()
    all_desk.draw_desk_by_edge()
    plt.ion()
    plt.pause(2)