#coding=utf-8

from modbus_rtu import ModbusRtu
import serial

class Infrared:
    def __init__(self, ir_sensor_id=0x03, read_length=19):
        self.ir_sensor_all_people = 0 #该红外传感器总人数
        self.ir_sensor_unit_people = [] #每个单元的人数
        self.ir_sensor_id = ir_sensor_id  # 当前读哪个传感器

        self.__read_length = read_length     # 读的寄存器个数
        self.__ser = serial.Serial(port='/dev/ttyUSB0', baudrate=38400, parity=serial.PARITY_ODD, timeout=0.5)
        self.__command_485 = [self.ir_sensor_id, 0x03, 0x00, 0x00, 0x00, 0x07]
        self.__modbus = ModbusRtu()
        self.__send_data = self.__modbus.add_crc(self.__command_485) # 加上crc校验

    def get_data(self):
        while 1:
            n = self.__ser.write(self.__send_data)
            if n != len(self.__send_data):
                print("====================Write Fail==============")
                continue
            get_str = self.__ser.read(self.__read_length)
            if len(get_str) == self.__read_length:  # 长度一样
                data = []
                for i in get_str:
                    data.append(ord(i))
                #  data中的是获得的数据，
                #  0是地址，1是function code,2是Byte Count
                # 3和4是人数，5，6，7，8是人的坐标，9，10是存在的数据，11，12，13，14是人的移动方向
                # 15，16是温度信息
                if self.__modbus.check_crc(data): # crc校验
                    self.ir_sensor_all_people = data[4]
                    human_coordinate = [data[5],data[6],data[7],data[8]]
                    self.ir_sensor_unit_people = self.__convert_data(self.ir_sensor_all_people, human_coordinate)
                    break
            else:
                print("==================FAIL=====================", len(get_str))

    def __convert_data(self, people_num, human_coordinate):
        IR_sensor_detect = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        for person in range(people_num):
            if person % 2 == 0:
                x = (human_coordinate[person // 2] & 0x03)
                y = (human_coordinate[person // 2] & 0x0c) >> 2
            else:
                x = (human_coordinate[person // 2] & 0x30) >> 4
                y = (human_coordinate[person // 2] & 0xc0) >> 6
            IR_sensor_detect[x + y * 4] += 1
        return IR_sensor_detect










