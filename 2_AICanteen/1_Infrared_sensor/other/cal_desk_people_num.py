import sys
sys.path.append('../')
from parameters import all_table_ir_sensor_relate

class count_desk_people():
    def __init__(self, desk_id, all_table_ir_sensor_relate=all_table_ir_sensor_relate):
        self.__desk_ir_sensor_relate = all_table_ir_sensor_relate
        self.desk_id = desk_id
        self.desk_people_number = 0

    def cal_desk_people_num(self, IR_sensor_detect):
        people_num = 0
        ir_sensor_id = 0
        for ir_sensor in self.__desk_ir_sensor_relate[self.desk_id]:
            if len(ir_sensor) == 0:
                continue
            for i in ir_sensor.keys():
                ir_sensor_id = i
            ir_sensor_unit = ir_sensor[ir_sensor_id]
            for j in ir_sensor_unit:
                people_num += IR_sensor_detect[str(ir_sensor_id)][j - 1]
        self.desk_people_number = people_num

