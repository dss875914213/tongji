# -*- coding: utf-8 -*-
import sys

sys.path.append('../')
import requests
import random
from parameters import dancing_robot_ip, dancing_robot_port
import logging


class Robot:
    def __init__(self):
        self.__action = 23  # 1 <= action <= 24

    def __set_action(self, action=23):
        if 1 <= action <= 24:
            self.__action = action

    def control_robot(self):
        used_action = [1, 2, 14, 16, 17, 18, 19, 20, 21, 23, 24]
        action = random.randint(0, 10)
        self.__set_action(used_action[action])
        try:
            res = requests.post("http://{}:{}/control".format(dancing_robot_ip, dancing_robot_port),
                                data={"target": 0, "awake": 1, "force_action": self.__action}, timeout=3)
            logging.info("与跳舞机器人通信后返回数据为：")
            logging.info(res.text)
        except Exception as e:
            logging.error("与跳舞机器人通信失败")
            logging.error(e)
