# -*- coding: utf-8 -*-

import sys
sys.path.append('../')
sys.path.append('communication')
from communication_modbus import ir_sensor_modbus
from parameters import ir_sensor_number,exist_ir_sensor,all_ir_sensor_unit_position
from parameters import error_ir_sensor_unit,detect_human_in_front_of_robot_ir_sensor_id
import logging
class _IrSensor:
    """
    输入：
        ir_sensor_id：红外传感器的ID
        ir_sensor_unit_position：红外传感器每个单元的位置
    对外属性：
        people_position：人的位置(一个位置有多个人，添加多个相同坐标)
        ir_sensor_unit_people_number：红外阵列传感器每个单元内的人数
        ir_sensor_detected_people_number：该红外阵列内人数(not_used)
    对外方法：
        get_all_data():得到所有需要的数据
    """
    def __init__(self,ir_sensor_id,ir_sensor_unit_position):
        self.__unit_number = 16
        self.__ir_sensor_id = ir_sensor_id
        self.__ir_sensor_unit_position = ir_sensor_unit_position
        self.people_position = None
        self.ir_sensor_unit_people_number = None
        self.ir_sensor_detected_people_number = None

    def __get_ir_sensor_data(self):
        ir_sensor_modbus.get_parsed_data(self.__ir_sensor_id)
        self.ir_sensor_detected_people_number = ir_sensor_modbus.ir_sensor_detected_people_number
        self.ir_sensor_unit_people_number = ir_sensor_modbus.ir_sensor_unit_people_number
        # 滤除错误的点
        if self.__ir_sensor_id in error_ir_sensor_unit.keys():
            for error_unit in error_ir_sensor_unit[self.__ir_sensor_id]:
                self.ir_sensor_detected_people_number -= self.ir_sensor_unit_people_number[error_unit-1]
                self.ir_sensor_unit_people_number[error_unit-1] = 0

    def __get_people_position(self):
        add = 0
        self.people_position = {}
        for unit in range(self.__unit_number):
            temp = self.ir_sensor_unit_people_number[unit]
            while temp:
                self.people_position[add] = self.__ir_sensor_unit_position[unit]
                add += 1
                temp -= 1

    def get_all_data(self):
        self.__get_ir_sensor_data()
        self.__get_people_position()


class AllIrSensor:
    """
    输入：
        ir_sensor_number：红外传感器的数量
        exist_ir_sensor：目前使用的红外传感器数量
        all_ir_sensor_unit_position：所有红外传感器单元位置
    对外属性：
        all_people_position：所有人的位置(一个位置有多个人，添加多个相同坐标)
        all_ir_sensor_unit_people_number：所有红外阵列传感器每个单元内的人数
        all_people_number：红外阵列覆盖区域内人数(not_used)
    对外方法：
        get_all_data():得到所有需要的数据
    """
    def __init__(self,ir_sensor_number,exist_ir_sensor,all_ir_sensor_unit_position):
        # state
        self.__all_ir_sensor_number = ir_sensor_number
        self.__exist_all_ir_sensor = exist_ir_sensor
        self.__all_ir_sensor_unit_position = all_ir_sensor_unit_position
        self.__all_ir_sensor = {}
        self.__detect_human_in_front_of_robot_ir_sensor_id = detect_human_in_front_of_robot_ir_sensor_id
        self.all_people_position = {}
        self.all_ir_sensor_unit_people_number = {}
        self.all_people_number = 0 # not_used 覆盖区域内总人数
        self.detect_human_in_front_of_robot_ir_sensor = None

        # function
        self.__create_ir_sensor()

    def __create_ir_sensor(self):
        for ir_sensor_id in self.__exist_all_ir_sensor:
            self.__all_ir_sensor[ir_sensor_id] = _IrSensor(ir_sensor_id, self.__all_ir_sensor_unit_position[ir_sensor_id])

    def __get_all_people_position(self):
        self.all_people_position = {}
        self.all_people_number = 0
        for ir_sensor_id in self.__exist_all_ir_sensor:
            self.__all_ir_sensor[ir_sensor_id].get_all_data()
            for i in self.__all_ir_sensor[ir_sensor_id].people_position:
                self.all_people_position[self.all_people_number] = self.__all_ir_sensor[ir_sensor_id].people_position[i]
                self.all_people_number += 1

    def __get_all_ir_sensor_unit_people_number(self):
        self.all_ir_sensor_unit_people_number = {}

        for ir_sensor_id in self.__exist_all_ir_sensor:
            self.all_ir_sensor_unit_people_number[ir_sensor_id] = \
                self.__all_ir_sensor[ir_sensor_id].ir_sensor_unit_people_number

        for ir_sensor_id in range(self.__all_ir_sensor_number):
            if ir_sensor_id not in self.__exist_all_ir_sensor:
                self.all_ir_sensor_unit_people_number[ir_sensor_id] = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

    def __judge_people_in_front_of_ir_sensor(self):
        self.detect_human_in_front_of_robot_ir_sensor = _IrSensor(self.__detect_human_in_front_of_robot_ir_sensor_id,
                              all_ir_sensor_unit_position[self.__detect_human_in_front_of_robot_ir_sensor_id])
        self.detect_human_in_front_of_robot_ir_sensor.get_all_data()

    def get_all_data(self):
        try:
            self.__get_all_people_position()
            self.__get_all_ir_sensor_unit_people_number()
            self.__judge_people_in_front_of_ir_sensor()
        except Exception as e:
            logging.error("可能是获取传感器数据出错")
            logging.error(e)


if __name__ == '__main__':
    ir_sensor = _IrSensor(2,all_ir_sensor_unit_position[2])
    ir_sensor.get_all_data()
    print(ir_sensor.ir_sensor_detected_people_number)
    pass


