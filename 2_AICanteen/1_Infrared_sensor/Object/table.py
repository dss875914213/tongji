# -*- coding: utf-8 -*-
from parameters import all_table_ir_sensor_relate,desk_number,exist_desk

class _Table:
    """
    输入：
        table_id：桌子的id
        table_ir_sensor_relate：桌子和红外阵列之间的对应关系
    对外属性：
        table_people_number：该桌子周围的人数
    对外方法：
        cal_desk_people_num():计算桌子周围人数
    """

    def __init__(self, table_id,table_ir_sensor_relate):
        self.__table_ir_sensor_relate = table_ir_sensor_relate
        self.table_people_number = 0

    def cal_desk_people_num(self, all_ir_sensor_unit_people_number):
        people_num = 0
        ir_sensor_id = 0
        for ir_sensor in self.__table_ir_sensor_relate:
            if len(ir_sensor) == 0:
                continue
            for i in ir_sensor.keys():
                ir_sensor_id = i
            ir_sensor_unit = ir_sensor[ir_sensor_id]
            for j in ir_sensor_unit:
                people_num += all_ir_sensor_unit_people_number[ir_sensor_id][j - 1]
        self.table_people_number = people_num


class AllTable:
    def __init__(self,all_table_ir_sensor_relate,desk_number,exist_desk):
        # state
        self.table_number = desk_number
        self.__exist_desk = exist_desk
        self.__all_table = {}
        self.__all_table_ir_sensor_relate = all_table_ir_sensor_relate
        self.per_table_people_number = {}

        # function
        self.__create_table()

    def __create_table(self):
        for table_id in exist_desk:
            self.__all_table[table_id] = _Table(table_id,self.__all_table_ir_sensor_relate[table_id])

    def cal_all_desk_people_number(self,all_ir_sensor_unit_people_number):
        self.per_table_people_number = {}
        for table_id in exist_desk:
            self.__all_table[table_id].cal_desk_people_num(all_ir_sensor_unit_people_number)
            self.per_table_people_number[table_id] = self.__all_table[table_id].table_people_number


