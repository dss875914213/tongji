# -*- coding: utf-8 -*-
from math import sin,cos

# 依据红外阵列传感器的中心高度和角度计算单元位置
def cal_ir_sensor_unit_position(ir_sensor_center_position,detect_area,ir_sensor_angle):
    unit_position = [[-0.375, 0.375], [-0.125, 0.375], [0.125, 0.375], [0.375, 0.375],
                    [-0.375, 0.125], [-0.125, 0.125], [0.125, 0.125], [0.375, 0.125],
                    [-0.375, -0.125], [-0.125, -0.125], [0.125, -0.125], [0.375, -0.125],
                    [-0.375, -0.375], [-0.125, -0.375], [0.125, -0.375], [0.375, -0.375]]

    unit_location = []
    for unit_id in range(16):
        temp_x = ir_sensor_center_position[0] + sin(ir_sensor_angle / 180 * 3.14) * (unit_position[unit_id][0] * detect_area) + \
                 cos(ir_sensor_angle / 180 * 3.14) * (unit_position[unit_id][1] * detect_area)

        temp_y = ir_sensor_center_position[1] - cos(ir_sensor_angle / 180 * 3.14) * (unit_position[unit_id][0] * detect_area) + \
                 sin(ir_sensor_angle / 180 * 3.14) * (unit_position[unit_id][1] * detect_area)
        unit_location.append([temp_x, temp_y])

    print(unit_location)

# 5号
# 5 (3.07,-18.90),3.737333333333333,0

# 高度转换边长公式：h/2.7*3.6

cal_ir_sensor_unit_position((4.79,-2.29),3.71,0)